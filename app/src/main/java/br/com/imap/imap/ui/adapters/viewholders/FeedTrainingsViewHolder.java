package br.com.imap.imap.ui.adapters.viewholders;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.TrainingsResponse;

/**
 * Created by Samuel Ribeiro on 04/01/2017.
 */

@EViewGroup(R.layout.adapter_treinamento)
public class FeedTrainingsViewHolder extends LinearLayout{

    Context context;

    @ViewById(R.id.tv_treinamento_titulo)
    TextView tvTreinamentoTitulo;
    @ViewById(R.id.tv_treinamento_type)
    TextView tvTreinamentoType;
    @ViewById(R.id.tv_treinamento_date)
    TextView tvTreinamentoDate;
    @ViewById(R.id.tv_score)
    TextView tvScore;


    public FeedTrainingsViewHolder(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(TrainingsResponse response) {
        tvTreinamentoTitulo.setText(response.getName());
        tvTreinamentoType.setText(response.getCategory());
        tvTreinamentoDate.setText(response.getCreatedAt());
    }
}
