package br.com.imap.imap.fcm;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import br.com.imap.imap.app.ImapApp;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refresh Token: "+refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken) {
        ImapApp.prefs.gcmToken().put(refreshedToken);
    }
}
