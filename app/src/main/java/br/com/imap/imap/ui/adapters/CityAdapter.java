package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import br.com.imap.imap.custom.ViewWrapper;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.models.response.RescuePointsResponse;
import br.com.imap.imap.ui.adapters.viewholders.CityViewHolder;
import br.com.imap.imap.ui.adapters.viewholders.CityViewHolder_;

/**
 * Created by Samuel Ribeiro on 13/01/2017.
 */
@EBean
public class CityAdapter extends RecyclerViewAdapterBase<RescuePointsResponse, CityViewHolder> {

    @RootContext
    Context context;
    protected List<RescuePointsResponse> rescuePoints;
    private RecyclerViewItemClick<RescuePointsResponse> listener;

    public void setOnItemClickListener(@NonNull RecyclerViewItemClick<RescuePointsResponse> listener) {
        this.listener = listener;
    }

    @Override
    protected CityViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return CityViewHolder_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<CityViewHolder> holder, int position) {

        CityViewHolder view = holder.getView();
        final RescuePointsResponse rescuePointsResponse = items.get(position);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClicked(view, rescuePointsResponse);
            }
        });
        view.bind(rescuePointsResponse);

    }
}
