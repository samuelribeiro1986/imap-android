package br.com.imap.imap.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import br.com.imap.imap.R;
import br.com.imap.imap.interfaces.SnackBarClickDismissInterface;
import br.com.imap.imap.ui.home.HomeActivity_;
import uk.co.chrisjenx.calligraphy.CalligraphyUtils;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public class DialogUtils {

    private final static int WIDGET_COLOR = R.color.colorPrimaryDark;
    private final static int TITLE_COLOR = R.color.colorPrimaryDark;
    private final static int CONTENT_COLOR = R.color.colorAccent;
    private Context context;

    public DialogUtils(@NonNull Context context) {
        this.context = context;
    }

    public static DialogUtils with(@NonNull Context context) {
        return new DialogUtils(context);
    }

    public static void ChoiceDialog(Context context, String[] items, String strTitle, MaterialDialog.ListCallbackSingleChoice listCallbackSingleChoice) {
        new MaterialDialog.Builder(context)
                .title(strTitle)
                .items((CharSequence[]) items)
                .widgetColorRes(WIDGET_COLOR)
                .titleColorRes(TITLE_COLOR)
                .itemsCallbackSingleChoice(0, listCallbackSingleChoice)
                .positiveText(R.string.ok)
                .theme(Theme.LIGHT)
                .show();
    }

    public static void createConfirmationDialog(Context context, int title, int message, int positiveText, int neutralText, final MaterialDialog.SingleButtonCallback listener) {

        new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .titleColorRes(TITLE_COLOR)
                .onPositive(listener)
                .onNeutral(listener)
                .positiveText(positiveText)
                .neutralText(neutralText)
                .titleColorRes(TITLE_COLOR)
                .contentColorRes(CONTENT_COLOR)
                .titleGravity(GravityEnum.CENTER)
                .contentGravity(GravityEnum.CENTER)
                .cancelable(false)
                .show();
    }

    public static void createConfirmationDialogWithoutTitle(Context context, int message, int positiveText, int neutralText, final MaterialDialog.SingleButtonCallback listener) {

        new MaterialDialog.Builder(context)
                .content(message)
                .titleColorRes(TITLE_COLOR)
                .onPositive(listener)
                .onNeutral(listener)
                .positiveText(positiveText)
                .neutralText(neutralText)
                .contentColorRes(CONTENT_COLOR)
                .contentGravity(GravityEnum.CENTER)
                .cancelable(false)
                .show();
    }

    public static void createConfirmationDialogWithoutTitle(Context context, String message, int positiveText, int negativeText, final MaterialDialog.SingleButtonCallback listener) {

        new MaterialDialog.Builder(context)
                .content(message)
                .titleColorRes(TITLE_COLOR)
                .onPositive(listener)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .titleColorRes(TITLE_COLOR)
                .contentColorRes(CONTENT_COLOR)
                .cancelable(false)
                .show();
    }

    public static void createLogOutConfirmation(Context context, int message, int positiveText, int negativeText, final MaterialDialog.SingleButtonCallback listener) {

        new MaterialDialog.Builder(context)
                .content(message)
                .onNegative(listener)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .titleColorRes(TITLE_COLOR)
                .contentColorRes(CONTENT_COLOR)
                .titleGravity(GravityEnum.CENTER)
                .cancelable(false)
                .show();
    }

    public static MaterialDialog createInformationDialog(Context context, int message, final MaterialDialog.SingleButtonCallback listener) {

        return new MaterialDialog.Builder(context)
                .content(message)
                .onNeutral(listener)
                .neutralText(R.string.ok)
                .titleColorRes(TITLE_COLOR)
                .contentColorRes(CONTENT_COLOR)
                .titleGravity(GravityEnum.CENTER)
                .contentGravity(GravityEnum.CENTER)
                .buttonsGravity(GravityEnum.CENTER)
                .cancelable(false)
                .show();
    }

    public static MaterialDialog createInformationDialog(Context context, int title, int message, final MaterialDialog.SingleButtonCallback listener) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .onNeutral(listener)
                .neutralText(R.string.ok)
                .titleColorRes(TITLE_COLOR)
                .contentColorRes(CONTENT_COLOR)
                .titleGravity(GravityEnum.CENTER)
                .contentGravity(GravityEnum.CENTER)
                .buttonsGravity(GravityEnum.CENTER)
                .cancelable(false)
                .show();
    }

    public static MaterialDialog createInformationDialog(Context context, String message, final MaterialDialog.SingleButtonCallback listener) {

        return new MaterialDialog.Builder(context)
                .content(message)
                .onNeutral(listener)
                .neutralText(R.string.ok)
                .titleColorRes(TITLE_COLOR)
                .contentColorRes(CONTENT_COLOR)
                .titleGravity(GravityEnum.CENTER)
                .contentGravity(GravityEnum.CENTER)
                .buttonsGravity(GravityEnum.CENTER)
                .cancelable(false)
                .show();
    }

    public static MaterialDialog createInformationDialogCustomButtonText(Context context, int message, int neutralText, final MaterialDialog.SingleButtonCallback listener) {

        return new MaterialDialog.Builder(context)
                .content(message)
                .onNeutral(listener)
                .neutralText(neutralText)
                .titleColorRes(TITLE_COLOR)
                .contentColorRes(CONTENT_COLOR)
                .titleGravity(GravityEnum.CENTER)
                .contentGravity(GravityEnum.CENTER)
                .buttonsGravity(GravityEnum.CENTER)
                .cancelable(false)
                .show();
    }

    public static MaterialDialog createMultiChoiceDialogDialog(Context context, int title, int positiveText, int negativeText, String[] items, final MaterialDialog.ListCallbackMultiChoice itemCallback, final MaterialDialog.SingleButtonCallback listener) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .items((CharSequence[]) items)
                .itemsCallbackMultiChoice(null, itemCallback)
                .onNeutral(listener)
                .onPositive(listener)
                .titleGravity(GravityEnum.CENTER)
                .positiveText(positiveText)
                .neutralText(negativeText)
                .widgetColorRes(WIDGET_COLOR)
                .cancelable(false)
                .show();
    }

    public static MaterialDialog createMultiChoiceDialogDialog(Context context, String title, int positiveText, String[] items, final MaterialDialog.ListCallbackMultiChoice itemCallback, final MaterialDialog.SingleButtonCallback listener) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .items((CharSequence[]) items)
                .itemsCallbackMultiChoice(null, itemCallback)
                .onNeutral(listener)
                .onPositive(listener)
                .titleGravity(GravityEnum.CENTER)
                .positiveText(positiveText)
                .widgetColorRes(WIDGET_COLOR)
                .cancelable(false)
                .show();
    }

    public static MaterialDialog createCustomDialog(Context context, int positiveText, int customView, final MaterialDialog.SingleButtonCallback listener) {

        return new MaterialDialog.Builder(context)
                .onNeutral(listener)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.CENTER)
                .neutralText(positiveText)
                .widgetColorRes(WIDGET_COLOR)
                .cancelable(false)
                .customView(customView, true).build();
    }

    public static MaterialDialog createCustomDialogWithoutButtons(Context context, int title, int customView) {

        return new MaterialDialog.Builder(context)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.START)
                .title(title)
                .widgetColorRes(WIDGET_COLOR)
                .cancelable(true)
                .customView(customView, false)
                .show();
    }

    public static MaterialDialog createCustomDialogWithoutButtons(Context context, int customView) {

        return new MaterialDialog.Builder(context)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.START)
                .widgetColorRes(WIDGET_COLOR)
                .cancelable(true)
                .customView(customView, false)
                .show();
    }

    public static MaterialDialog createCustomDialogWithoutButtons(final Context context, int customView, String pontuacao) {

        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.START)
                .widgetColorRes(WIDGET_COLOR)
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        HomeActivity_.intent(context)
                                .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                .start();
                    }
                })
                .cancelable(true)
                .customView(customView, false)
                .show();

        TextView pontos = (TextView) dialog.findViewById(R.id.tv_treinamento_pontos);
        pontos.setText(context.getString(R.string.pontos_ganhos).replace("{x}", pontuacao));


        return dialog;
    }

    public static MaterialDialog createCustomDialogWithoutButtonsWithScroll(Context context, int customView) {

        return new MaterialDialog.Builder(context)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.START)
                .widgetColorRes(WIDGET_COLOR)
                .cancelable(true)
                .customView(customView, true).build();
    }

    public static void showSnack(final Context context, View view, String title, final SnackBarClickDismissInterface listener) {


        try {
            Snackbar snackBar = Snackbar.make(((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content), title, Snackbar.LENGTH_LONG);
            snackBar.setAction(context.getText(R.string.ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick();
                }
            });
            snackBar.setCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    super.onDismissed(snackbar, event);
                    listener.onDismissed();
                }
            });
            snackBar.setActionTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            snackBar.show();

            View snackBarView = snackBar.getView();
            snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            snackBarView.setMinimumHeight(80);
            TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            textView.setMaxLines(10);

            TextView textViewAction = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_action);
            CalligraphyUtils.applyFontToTextView(context, textViewAction, context.getString(R.string.fontAvenirBook));

        } catch (Exception ignored) {

        }
    }


    public static void showSnackBarWithoutConfirmation(Context context, int text) {
        showSnack(context, ((Activity) context).findViewById(android.R.id.content), context.getString(text), new SnackBarClickDismissInterface() {
            @Override
            public void onClick() {

            }

            @Override
            public void onDismissed() {

            }
        });
    }

    public MaterialDialog.Builder getDialog(int title, int content) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content);

    }

    public MaterialDialog.Builder getDialogWithoutTitle(int content) {

        return new MaterialDialog.Builder(context)
                .content(content);
    }

    public MaterialDialog.Builder getDialog(
            int title, int content, int positiveText) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .positiveText(positiveText);

    }

    public MaterialDialog.Builder getDialog(
            String title, String content, String positiveText) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .positiveText(positiveText);

    }

    public MaterialDialog.Builder getDialogNonCancelable(
            String title, String content, String positiveText) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .cancelable(false)
                .positiveText(positiveText);

    }

    public MaterialDialog.Builder getDialog(
            int title, int content, int positiveText, int negativeText) {

        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .positiveText(positiveText)
                .negativeText(negativeText);

    }

    public MaterialDialog.Builder getNetworkErrorDialog() {

        return getDialog(R.string.dialog_no_connection_title,
                R.string.dialog_no_connection_content,
                R.string.dialog_no_connection_try_again,
                R.string.dialog_no_connection_cancel)
                .cancelable(false);

    }

    public MaterialDialog.Builder getDefaultProgressBar() {

        return getDialogWithoutTitle(
                R.string.please_wait)
                .progress(true, 0);

    }

    public MaterialDialog.Builder getUnexpectedErrorDialog(String detail) {
        return getDialog(
                context.getString(R.string.unexpected_error_title),
                String.format(context.getString(R.string.unexpected_error_content_format), detail),
                context.getString(R.string.dialog_close));
    }
}
