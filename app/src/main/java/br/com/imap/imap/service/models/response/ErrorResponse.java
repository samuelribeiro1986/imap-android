package br.com.imap.imap.service.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.imap.imap.service.models.BaseModel;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public class ErrorResponse extends BaseModel {

    private static final long serialVersionUID = 6028923781046789201L;

    @SerializedName("errors")
    @Expose
    private List<String> errors = new ArrayList<>();


    /**
     *
     * @return
     * The errors
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     *
     * @param errors
     * The errors
     */
    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

}
