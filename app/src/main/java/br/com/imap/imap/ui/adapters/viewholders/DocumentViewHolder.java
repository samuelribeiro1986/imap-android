package br.com.imap.imap.ui.adapters.viewholders;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.Document;
import br.com.imap.imap.utils.GlideUtils;

/**
 * Created by Samuel on 07/01/2017.
 */
@EViewGroup(R.layout.adapter_document)
public class DocumentViewHolder extends LinearLayout {

    Context mContext;
    @ViewById(R.id.iv_document_type)
    ImageView ivDocumentType;
    @ViewById(R.id.tv_document_title)
    TextView tvDocumentTitle;
    @ViewById(R.id.tv_document_description)
    TextView tvDocumentDescription;

    public DocumentViewHolder(Context context) {
        super(context);
        mContext = context;
    }

    public void bind(Document document) {

        GlideUtils.loadImage(document.getFile().getUrl(), mContext, ivDocumentType, R.drawable.ic_pdf, 30, 10);
        tvDocumentTitle.setText(document.getName());
        tvDocumentDescription.setText(document.getDescription());

    }
}
