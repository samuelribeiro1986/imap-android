package br.com.imap.imap.enums;

/**
 * Created by Samuel Ribeiro on 10/01/2017.
 */

public enum StatusEnum {

    WAITING_RESCUE(0, "Aguardando resgate", "waiting_rescue"),
    RESCUED(1, "Resgatado", "rescued");

    private int id;
    private String value;
    private String name;

    StatusEnum(int id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public static String getNameByValue(String value) {
        for (StatusEnum c :
                StatusEnum.values()) {
            if (c.getValue().equals(value)) return c.geName();
        }
        return WAITING_RESCUE.geName();
    }

    public int getId() {
        return id;
    }

    public String geName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
