package br.com.imap.imap.ui.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.custom.RecyclerViewEmptySupport;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.NotificationsResponse;
import br.com.imap.imap.service.models.response.Rescues;
import br.com.imap.imap.ui.adapters.NotificationsAdapter;
import br.com.imap.imap.ui.adapters.RescuesAdapter;
import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;

@EActivity(R.layout.activity_notifications)
public class NotificationsActivity extends BaseActivity implements RecyclerViewItemClick<NotificationsResponse> {

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;

    @ViewById(R.id.empty_notif_results_view)
    View emptyView;

    @ViewById(R.id.notif_list)
    RecyclerViewEmptySupport myList;

    @ViewById(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayoutManager lm;

    @Bean
    NotificationsAdapter notificationsAdapter;

    int pastVisibleItems, visibleItemCount, totalItemCount;
    private List<NotificationsResponse> notificationsResponses;
    private boolean loading = true;
    private boolean isCallingApi = false;
    private int currPage = 1;
    private boolean morePages = true;

    @AfterViews
    public void afterViews() {
        setupToolbar(getString(R.string.premios_resgatados), true, false);
        populateRescues();
    }

    private void populateRescues() {
        setupPropertiesAdapter();

        if (notificationsResponses != null && notificationsResponses.size() > 0) {
            notificationsAdapter.setData(notificationsResponses);
        } else {
            getNotifications(1);
        }
    }

    private void setupPropertiesAdapter() {

        lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        myList.setLayoutManager(lm);
        notificationsAdapter.setOnItemClickListener(this);

        myList.setAdapter(notificationsAdapter);
        myList.setEmptyView(emptyView);

        myList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = lm.getChildCount();
                    totalItemCount = lm.getItemCount();
                    pastVisibleItems = lm.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            if (morePages && !isCallingApi) {
                                currPage++;
                                getNotifications(currPage);
                            }
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currPage = 1;
                morePages = true;
                getNotifications(currPage);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
    }

    public void getNotifications(final int pageNumber) {
        loading = true;
        ImapApp.getApiService().getNotifications(currPage).enqueue(new MyCallbackApi<List<NotificationsResponse>, BaseActivity>(this) {

            @Override
            public void after() {
                if (emptyView == null) return;
                isCallingApi = false;
                if (progressView != null && myList != null && swipeRefreshLayout != null) {
                    progressView.setVisibility(View.GONE);
                    myList.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void before() {
                if (emptyView == null) return;
                isCallingApi = true;
                emptyView.setVisibility(View.GONE);
                if (!swipeRefreshLayout.isRefreshing()) {
                    progressView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(Response<List<NotificationsResponse>> response) {
                notificationsResponses = response.body();
                if (notificationsResponses.size() == 0) {
                    morePages = false;
                }
                notificationsAdapter.setData(notificationsResponses);
            }

            @Override
            public void onUnexpectedError(Call<List<NotificationsResponse>> call, Throwable t) {
                handleUnexpectedError(t);
            }
        });
    }

    @Override
    public void onClicked(View view, NotificationsResponse item) {

    }
}
