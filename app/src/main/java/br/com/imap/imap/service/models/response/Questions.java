package br.com.imap.imap.service.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.imap.imap.service.models.BaseModel;

/**
 * Created by Samuel Ribeiro on 04/01/2017.
 */

public class Questions extends BaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("user_answers")
    @Expose
    private UserAnswers userAnswers;
    @SerializedName("options")
    @Expose
    private List<Option> options;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public UserAnswers getUserAnswers() {
        return userAnswers;
    }

    public void setUserAnswers(UserAnswers userAnswers) {
        this.userAnswers = userAnswers;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }
}
