package br.com.imap.imap.service.models.request;

import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public class LoginRequest extends BaseModel {

    @SerializedName("auth")
    private String auth;

    @SerializedName("password")
    private String password;

    @SerializedName("metadata")
    private MetadataRequest metadataRequest;

    public LoginRequest(String auth, String password, MetadataRequest metadataRequest) {
        this.auth = auth;
        this.password = password;
        this.metadataRequest = metadataRequest;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MetadataRequest getMetadataRequest() {
        return metadataRequest;
    }

    public void setMetadataRequest(MetadataRequest metadataRequest) {
        this.metadataRequest = metadataRequest;
    }
}
