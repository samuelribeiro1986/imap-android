package br.com.imap.imap.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.ui.home.HomeActivity_;
import br.com.imap.imap.ui.intro.LoginActivity_;
import br.com.imap.imap.utils.GlideUtils;

@EActivity(R.layout.activity_second_splash)
public class SecondSplashActivity extends Activity {

    @ViewById(R.id.iv_splash)
    ImageView ivSplash;

    @Extra(Constants.EXTRA.SPLASH)
    String splashUrl;

    private static final int SPLASH_TIME = 3000;

    @AfterViews
    public void afterViews(){
        GlideUtils.loadImage(splashUrl, this, ivSplash);
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                LoginActivity_.intent(SecondSplashActivity.this)
                        .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .start();
                finish();
            }
        }, SPLASH_TIME);
    }

}
