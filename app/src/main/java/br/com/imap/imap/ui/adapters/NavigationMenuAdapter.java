package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;

import br.com.imap.imap.R;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.models.User;
import br.com.imap.imap.ui.activities.AboutActivity_;
import br.com.imap.imap.ui.activities.NotificationsActivity_;
import br.com.imap.imap.ui.activities.RescuedPrizesActivity_;
import br.com.imap.imap.ui.home.HomeActivity;
import br.com.imap.imap.utils.GlideUtils;

/**
 * Created by Samuel Ribeiro on 09/01/2017.
 */
public class NavigationMenuAdapter extends RecyclerView.Adapter<NavigationMenuAdapter.ViewHolder> {

    String[] titles;
    Context context;
    User user;

    public NavigationMenuAdapter(String[] titles, Context context, User user) {
        this.titles = titles;
        this.context = context;
        this.user = user;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        TextView navigationMenuTitle;
        ImageView ivUserImage;
        TextView tvUserName;
        TextView tvUserPoints;
        View divider;

        public ViewHolder(View drawerItem, int itemType, Context context) {
            super(drawerItem);
            this.context = context;
            drawerItem.setOnClickListener(this);
            if (itemType == 1) {
                navigationMenuTitle = (TextView) itemView.findViewById(R.id.tv_NavTitle);
                divider = itemView.findViewById(R.id.divider);
            } else if ( itemType == 0 ) {
                tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
                tvUserPoints = (TextView) itemView.findViewById(R.id.tv_user_points);
                ivUserImage = (ImageView) itemView.findViewById(R.id.iv_user_image);
            }

        }

        @Override
        public void onClick(View view) {
            HomeActivity homeActivity = (HomeActivity) context;
//            homeActivity.drawerLayout.closeDrawers();

            switch (getAdapterPosition()) {
                case 1:
                    NotificationsActivity_.intent(context).start();
                    break;
                case 2:
                    RescuedPrizesActivity_.intent(context).start();
                    break;
                case 3:
                    AboutActivity_.intent(context).start();
                    break;
            }
        }
    }

    @Override
    public NavigationMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (viewType == 1) {
            View itemLayout = layoutInflater.inflate(R.layout.navigation_drawer_item, null);
            return new ViewHolder(itemLayout, viewType, context);
        } else if (viewType == 0) {
            View itemHeader = layoutInflater.inflate(R.layout.navigation_drawer_header, null);
            return new ViewHolder(itemHeader, viewType, context);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0) {
            GlideUtils.loadImage(user.getAvatar(), context, holder.ivUserImage, R.drawable.user_header_place_holder);
            holder.tvUserName.setText(user.getName());
            holder.tvUserPoints.setText(context.getString(R.string.pontos_disponiveis).replace("{x}", user.getPoints().toString()));
        } else if (position != 0) {
            holder.navigationMenuTitle.setText(titles[position - 1]);
            if (position == titles.length) {
                holder.divider.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return titles.length + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return 0;
        else return 1;
    }
}
