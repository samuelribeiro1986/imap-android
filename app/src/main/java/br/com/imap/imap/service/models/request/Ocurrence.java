package br.com.imap.imap.service.models.request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import br.com.imap.imap.service.models.BaseModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Samuel Ribeiro on 11/01/2017.
 */

public class Ocurrence extends BaseModel {

    @SerializedName("local")
    @Expose
    private String local;
    @SerializedName("picture")
    private File picture;
    @SerializedName("description")
    @Expose
    private String description;

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public File getPicture() {
        return picture;
    }

    public void setPicture(File picture) {
        this.picture = picture;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, RequestBody> getMultipart() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(this, Ocurrence.class);

        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> map = gson.fromJson(json, type);

        Map<String, RequestBody> body = new HashMap<>();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            body.put(entry.getKey(),
                    RequestBody.create(MediaType.parse("text/plain"), entry.getValue()));
        }

        return body;
    }

    public MultipartBody.Part getMultipartPhoto() {
        MultipartBody.Part image = null;
        if (picture != null) {
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("image/*"), picture);
            image =
                    MultipartBody.Part.createFormData("picture", picture != null ? picture.getName() : "", requestFile);
        }
        return image;
    }
}
