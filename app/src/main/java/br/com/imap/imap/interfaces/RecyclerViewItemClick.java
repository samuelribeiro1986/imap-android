package br.com.imap.imap.interfaces;

import android.view.View;

/**
 * Created by Samuel on 07/01/2017.
 */

public interface RecyclerViewItemClick<T> {

    void onClicked(View view, T item);


}