package br.com.imap.imap.service;

import java.util.List;
import java.util.Map;

import br.com.imap.imap.app.Constants.API;
import br.com.imap.imap.service.models.request.Ocurrence;
import br.com.imap.imap.service.models.request.RescueRequest;
import br.com.imap.imap.service.models.response.MapPointsResponse;
import br.com.imap.imap.service.models.response.NotificationsResponse;
import br.com.imap.imap.service.models.response.RescuePoint;
import br.com.imap.imap.service.models.response.RescuePointsResponse;
import br.com.imap.imap.service.models.response.Rescues;
import br.com.imap.imap.service.models.User;
import br.com.imap.imap.service.models.request.LoginRequest;
import br.com.imap.imap.service.models.response.ProductsResponse;
import br.com.imap.imap.service.models.response.RankingResponse;
import br.com.imap.imap.service.models.response.SplashResponse;
import br.com.imap.imap.service.models.response.TrainingsResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public interface MyApiEndpointInterface {

    @POST(API.Account.SIGNIN)
    Call<User> signin(@Body LoginRequest loginRequest);

    //*******  Trainings *************
    @GET(API.Trainings.INDEX)
    Call<List<TrainingsResponse>> getListTrainings(@Query("page") int page);


    //*******  Ranking *************
    @GET(API.Ranking.TOP_TEN)
    Call<List<RankingResponse>> getTopTenRank () ;

    @GET(API.Ranking.MY_RANK)
    Call<List<RankingResponse>> getMyRank ();

    //*******  Products *************
    @GET(API.Products.INDEX)
    Call<List<ProductsResponse>> getListProducts(@Query("page") int page);

    //*******  Rescues *************
    @POST(API.Rescues.CREATE)
    Call<Rescues> rescuePrize (@Body RescueRequest rescueRequest);

    @GET(API.Rescues.INDEX)
    Call<List<Rescues>> getRescues ();

    @GET(API.RescuePoints.INDEX)
    Call<List<RescuePointsResponse>> getRescuePoints ();

    @GET(API.RescuePoints.INDEX)
    Call<List<RescuePointsResponse>> getRescueCities (@Query("state") String state);

    //*******  Notifications *************
    @GET(API.Notifications.INDEX)
    Call<List<NotificationsResponse>> getNotifications (@Query("page") int page);

    //*******  MapPins *************
    @GET(API.MapPoints.INDEX)
    Call<List<MapPointsResponse>> getMapPoints ();

    //*******  Ocurrence *************
    @Multipart
    @POST(API.Ocurrence.CREATE)
    Call<Ocurrence> createOcurrence(@PartMap Map<String, RequestBody> ocurrence, @Part MultipartBody.Part photo);

    //*******  Splash *************
    @GET(API.Splash.INDEX)
    Call<List<SplashResponse>> getSplashes ();






}
