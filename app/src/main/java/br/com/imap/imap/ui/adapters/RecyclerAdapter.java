package br.com.imap.imap.ui.adapters;

/**
 * Created by Samuel on 04/01/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.service.models.response.TrainingsResponse;
import br.com.imap.imap.ui.activities.YouTubePlayerActivity_;
import br.com.imap.imap.utils.Utils;

/**
 * Created by ofaroque on 8/13/15.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.VideoInfoHolder> {

    Context ctx;
    List<TrainingsResponse> trainingsResponses;

    public RecyclerAdapter(Context context, List<TrainingsResponse> trainingsResponses) {
        this.ctx = context;
        this.trainingsResponses = trainingsResponses;
    }

    @Override
    public VideoInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_youtube_cardview, parent, false);
        return new VideoInfoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoInfoHolder holder, final int position) {


        final YouTubeThumbnailLoader.OnThumbnailLoadedListener onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
            @Override
            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

            }

            @Override
            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                youTubeThumbnailView.setVisibility(View.VISIBLE);
                holder.relativeLayoutOverYouTubeThumbnailView.setVisibility(View.VISIBLE);
            }
        };

        holder.youTubeThumbnailView.initialize(ctx.getResources().getString(R.string.api_key), new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                youTubeThumbnailLoader.setVideo(getYoutubeVideoId(trainingsResponses.get(position).getVideoUrl()));
                youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                //write something for failure
            }
        });

        holder.tvTreinamentoTitulo.setText(trainingsResponses.get(position).getName());
        holder.tvTreinamentoSubtitulo.setText(trainingsResponses.get(position).getCategory());
        holder.tvTreinamentoDate.setText(Utils.getFormattedDate(trainingsResponses.get(position).getCreatedAt()));
        holder.tvTreinamentoPontos.setText(trainingsResponses.get(position).getQuestions().getPoints().toString());
    }

    public String getYoutubeVideoId(String videoUrl) {
        String segments[] = videoUrl.split("=");
        return segments[1];
    }

    @Override
    public int getItemCount() {
        return trainingsResponses.size();
    }

    public class VideoInfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected RelativeLayout relativeLayoutOverYouTubeThumbnailView;
        protected ImageView playButton;
        protected TextView tvTreinamentoTitulo;
        protected TextView tvTreinamentoSubtitulo;
        protected TextView tvTreinamentoDate;
        protected TextView tvTreinamentoPontos;

        YouTubeThumbnailView youTubeThumbnailView;

        public VideoInfoHolder(View itemView) {
            super(itemView);
            playButton = (ImageView) itemView.findViewById(R.id.btnYoutube_player);
            playButton.setOnClickListener(this);
            relativeLayoutOverYouTubeThumbnailView = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_over_youtube_thumbnail);
            youTubeThumbnailView = (YouTubeThumbnailView) itemView.findViewById(R.id.youtube_thumbnail);
            tvTreinamentoTitulo = (TextView) itemView.findViewById(R.id.tv_treinamento_titulo);
            tvTreinamentoSubtitulo = (TextView) itemView.findViewById(R.id.tv_treinamento_subtitulo);
            tvTreinamentoDate = (TextView) itemView.findViewById(R.id.tv_treinamento_date);
            tvTreinamentoPontos = (TextView) itemView.findViewById(R.id.tv_treinamento_pontos);
        }

        @Override
        public void onClick(View v) {

            YouTubePlayerActivity_.intent(ctx)
                    .extra(Constants.EXTRA.QUESTIONS, trainingsResponses.get(getLayoutPosition()).getQuestions())
                    .extra(Constants.EXTRA.VIDEO_ID, getYoutubeVideoId(trainingsResponses.get(getLayoutPosition()).getVideoUrl()))
                    .start();

        }
    }
}