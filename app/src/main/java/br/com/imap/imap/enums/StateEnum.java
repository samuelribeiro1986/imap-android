package br.com.imap.imap.enums;

/**
 * Created by Samuel Ribeiro on 13/01/2017.
 */

public enum StateEnum {

    MG(0, "Minas Gerais", "MG"),
    SP(1, "São Paulo", "SP"),
    RJ(2, "Rio de Janeiro", "RJ"),
    ES(3, "Espírito Santo", "ES"),
    BA(4, "Bahia", "BA"),
    CE(5, "Ceará", "CE"),
    SE(6, "Sergipe", "SE"),
    PR(7, "Paraná", "PR"),
    SC(8, "Santa Catarina", "SC"),
    RS(9, "Rio Grande do Sul", "RS");

    private int id;
    private String name;
    private String value;

    StateEnum(int id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public static String getValueById(int id) {
        for (StateEnum c :
                StateEnum.values()) {
            if (c.getId() == id) return c.getValue();
        }
        return MG.getValue();
    }

    public static String getNameByValue(String value) {
        for (StateEnum c :
                StateEnum.values()) {
            if (c.getValue().equals(value)) return c.getName();
        }
        return MG.getName();
    }

    public static int getIdByValue(String value) {
        for (StateEnum c :
                StateEnum.values()) {
            if (c.getValue().equals(value)) return c.getId();
        }
        return MG.getId();
    }

    public static String getValueByName(String name) {
        for (StateEnum c :
                StateEnum.values()) {
            if (c.getName().equals(name)) return c.getValue();
        }
        return MG.getName();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
