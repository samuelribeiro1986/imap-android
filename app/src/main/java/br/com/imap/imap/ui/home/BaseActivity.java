package br.com.imap.imap.ui.home;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;

import java.io.IOException;
import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.interfaces.LoginDialogCallback;
import br.com.imap.imap.interfaces.SnackBarClickDismissInterface;
import br.com.imap.imap.service.NetworkRetryRequest;
import br.com.imap.imap.service.models.User;
import br.com.imap.imap.service.models.response.ErrorResponse;
import br.com.imap.imap.service.models.response.LoginResponse;
import br.com.imap.imap.utils.AnimationsUtils;
import br.com.imap.imap.utils.DialogUtils;
import br.com.imap.imap.utils.NetworkUtils;
import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@EActivity
public class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;

    private MaterialDialog progressDialog;
    private LoginDialogCallback dialogLoginListener;

    @App
    public ImapApp app;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setupToolbar(boolean isBack) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
            setSupportActionBar(toolbar);
            if (isBack) {
                toolbar.setNavigationIcon(R.drawable.ic_voltar);
            }
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }
    }

    public void setupToolbar(String title, boolean isback, boolean isMain) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
            setSupportActionBar(toolbar);
            TextView textView = (TextView) toolbar.findViewById(R.id.tv_toolbar_title);
            textView.setText(title);
            ImageView imageView = (ImageView) toolbar.findViewById(R.id.iv_na_estrada);
            textView.setTextColor(ContextCompat.getColor(BaseActivity.this, R.color.blue_login_button));
            if (isback) {
                toolbar.setNavigationIcon(R.drawable.ic_voltar);
            }
            if (isMain) {
                textView.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
            }
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }
    }

    public void hideToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(View.GONE);
        }
    }

    public void changeTitleToolbar(String txtString) {
        if (toolbar != null) {
            TextView textView = (TextView) toolbar.findViewById(R.id.tv_toolbar_title);
            textView.setText(txtString);
        }
    }

    public void handleUnAuthorizedRequest(Response response) {
        logoutUnathorizedUser();
    }

    private void logoutUnathorizedUser() {
        DialogUtils.with(this).getDialogNonCancelable(null, getString(R.string.not_authorized_error), getString(R.string.ok)).onAny(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                logout();
            }
        });
    }

    private void logout() {
        ImapApp.prefs.clear();
        ImapApp.prefs.tutorialShown().put(true);
        HomeActivity_.intent(BaseActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
    }

    public void handleAPIErrors(ResponseBody responseBody) {

        try {
            handleAPIErrors(responseBody.string());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleAPIErrors(String responseString){
        ErrorResponse errorResponse = null;
        Gson gson = new Gson();

        try {
            errorResponse = gson.fromJson(responseString, ErrorResponse.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String message;
        if ( errorResponse == null || errorResponse.getErrors().size() == 0) {
            message = getString(R.string.unexpected_error_content);
        } else {
            message = errorResponse.getErrors().get(0);
        }

        try {
            DialogUtils.with(this).getDialog(null, message,
                    getString(R.string.dialog_close)).build().show();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void handleNotFoundError( Response response){

    }

    public void onNetworkError(final NetworkRetryRequest networkRetryRequest) {
        if (getApplicationContext() == null) return;

        try {
            DialogUtils.with(this).getNetworkErrorDialog()
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            networkRetryRequest.retry();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).build().show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateHeaderData(Headers headers) {
        if ( headers.get(Constants.HEADER.ACCESS_TOKEN) != null ) {
            ImapApp.prefs.accessToken().put(headers.get(Constants.HEADER.ACCESS_TOKEN));
            ImapApp.prefs.client().put(headers.get(Constants.HEADER.CLIENT));
            ImapApp.prefs.uid().put(headers.get(Constants.HEADER.UID));
        }
    }

    public void shakeErrors(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            AnimationsUtils.shakeError(view);
        }
    }

    public void handleUnexpectedError(Throwable t) {
        String message = t.getMessage();
        if ( message == null)
            message = t.toString();

        DialogUtils.with(this)
                .getUnexpectedErrorDialog(message)
                .build()
                .show();
    }

    public void showProgressDialog(){
        if (progressDialog == null) {

            progressDialog = DialogUtils.with(this)
                    .getDefaultProgressBar()
                    .build();
        }

        progressDialog.setCancelable(false);

        try {
            progressDialog.show();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        if ( progressDialog == null ) {
            progressDialog = DialogUtils.with(this)
                    .getDefaultProgressBar()
                    .build();
        }

        progressDialog.setCancelable(false);

        try {
            progressDialog.dismiss();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean checkInternetAndAlertUser() {
        if (!NetworkUtils.isInternetAvailable(this)) {
            DialogUtils.showSnack(BaseActivity.this, findViewById(android.R.id.content), getString(R.string.dialog_no_connection_content), new SnackBarClickDismissInterface() {
                @Override
                public void onClick() {

                }

                @Override
                public void onDismissed() {

                }
            });
            return true;
        }
        return false;
    }

    public void loginSuccessAndRedirect(User user){
        loginSuccess(user);
        finish();
        HomeActivity_.intent(BaseActivity.this).flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
    }

    private void loginSuccess(User user) {
        ImapApp.prefs.userId().put(user.getId().toString());
        app.setCurrentUser(user);
    }

    public void askForEnableGps(Context context) {
        DialogUtils.createConfirmationDialog(context, R.string.gps_offline_title, R.string.gps_offline_message, R.string.yes, R.string.no, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if ( which == DialogAction.POSITIVE ) {
                    Intent myIntent = new Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(myIntent, Constants.REQUEST_GPS);
                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    public void addressNotFoundSnack() {
        DialogUtils.showSnackBarWithoutConfirmation(this, R.string.no_address_found);
    }

}
