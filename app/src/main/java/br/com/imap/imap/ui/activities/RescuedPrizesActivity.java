package br.com.imap.imap.ui.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.custom.RecyclerViewEmptySupport;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.Rescues;
import br.com.imap.imap.ui.adapters.RescuesAdapter;
import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;

@EActivity(R.layout.activity_rescued_prizes)
public class RescuedPrizesActivity extends BaseActivity implements RecyclerViewItemClick<Rescues> {

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;

    @ViewById(R.id.empty_notif_results_view)
    View emptyView;

    @ViewById(R.id.rescues_list)
    RecyclerViewEmptySupport myList;

    @ViewById(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayoutManager lm;

    @Bean
    RescuesAdapter rescuesAdapter;

    MaterialDialog rateDialog;
    private List<Rescues> rescuesResponse;
    private boolean loading = true;
    private boolean isCallingApi = false;
    private int currPage = 1;
    private boolean morePages = true;

    @AfterViews
    public void afterViews() {
        setupToolbar(getString(R.string.premios_resgatados), true, false);
        populateRescues();
    }

    private void populateRescues() {
        setupPropertiesAdapter();

        if (rescuesResponse != null && rescuesResponse.size() > 0) {
            rescuesAdapter.setData(rescuesResponse);
        } else {
            getMyRescues();
        }
    }


    private void setupPropertiesAdapter() {

        lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        myList.setLayoutManager(lm);
        rescuesAdapter.setOnItemClickListener(this);

        myList.setAdapter(rescuesAdapter);
        myList.setEmptyView(emptyView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currPage = 1;
                morePages = true;
                getMyRescues();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
    }

    public void getMyRescues() {
        loading = true;
        ImapApp.getApiService().getRescues().enqueue(new MyCallbackApi<List<Rescues>, BaseActivity>(this) {

            @Override
            public void after() {
                if (emptyView == null) return;
                isCallingApi = false;
                if (progressView != null && myList != null && swipeRefreshLayout != null) {
                    progressView.setVisibility(View.GONE);
                    myList.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void before() {
                if (emptyView == null) return;
                isCallingApi = true;
                emptyView.setVisibility(View.GONE);
                if (!swipeRefreshLayout.isRefreshing()) {
                    progressView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(Response<List<Rescues>> response) {
                rescuesResponse = response.body();
                if (rescuesResponse.size() == 0) {
                    morePages = false;
                }
                rescuesAdapter.setData(rescuesResponse);
            }

            @Override
            public void onUnexpectedError(Call<List<Rescues>> call, Throwable t) {
                handleUnexpectedError(t);
            }
        });
    }


    @Override
    public void onClicked(View view, Rescues item) {

    }
}
