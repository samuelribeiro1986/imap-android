package br.com.imap.imap.ui.intro;

import android.text.Html;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.ui.home.BaseActivity;

@EActivity(R.layout.activity_termo)
public class TermoActivity extends BaseActivity {

    @ViewById(R.id.tv_tos)
    TextView tvTos;


    @AfterViews
    public void afterView() {
        setupToolbar(getString(R.string.use_terms), true, false);
        tvTos.setText(Html.fromHtml(getString(R.string.privacy)));
    }
}
