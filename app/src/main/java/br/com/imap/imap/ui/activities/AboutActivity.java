package br.com.imap.imap.ui.activities;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import br.com.imap.imap.R;
import br.com.imap.imap.ui.home.BaseActivity;

@EActivity(R.layout.activity_about)
public class AboutActivity extends BaseActivity {

    @AfterViews
    public void afterView(){
        setupToolbar(getString(R.string.about_imap), true, false);
    }


}
