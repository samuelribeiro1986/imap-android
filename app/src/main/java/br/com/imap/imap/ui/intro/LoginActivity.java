package br.com.imap.imap.ui.intro;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Type;
import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.User;
import br.com.imap.imap.service.models.request.LoginRequest;
import br.com.imap.imap.service.models.request.MetadataRequest;
import br.com.imap.imap.service.models.response.LoginResponse;
import br.com.imap.imap.service.models.response.RankingResponse;
import br.com.imap.imap.ui.home.BaseActivity;
import br.com.imap.imap.utils.Utils;
import retrofit2.Call;
import retrofit2.Response;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity implements Validator.ValidationListener {

    @ViewById(R.id.edt_email)
    @NotEmpty
    @Email
    EditText edtEmail;

    @ViewById(R.id.edt_senha)
    @NotEmpty
    @Password(min = 6)
    EditText edtSenha;

    @ViewById(R.id.tv_esqueci_senha)
    TextView tvEsqueciSenha;

    @ViewById(R.id.btn_login)
    Button btnLogin;

    Validator validator;

    @AfterViews
    public void afterView() {
        setupToolbar(getString(R.string.entrar), true, false);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Click(R.id.btn_login)
    public void loginClicked() {
        validator.validate();
    }

    @Click(R.id.tv_esqueci_senha)
    public void esqueciSenha() {
        EsqueciSenhaActivity_.intent(this).start();
    }


    @Override
    public void onValidationSucceeded() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        login(refreshedToken);

    }

    public void login(String deviceId){

        ImapApp.getApiService().signin(new LoginRequest(edtEmail.getText().toString(), edtSenha.getText().toString(), new MetadataRequest(deviceId, Constants.EXTRA.DEVICE_OS))).enqueue(new MyCallbackApi<User, BaseActivity>(this) {

            @Override
            public void before() {
                showProgressDialog();
            }

            @Override
            public void after() {
                hideProgressDialog();
            }

            @Override
            public void onSuccess(Response<User> response) {
                User user = response.body();
                loginSuccessAndRedirect(user);
            }

            @Override
            public void onUnexpectedError(Call<User> call, Throwable t) {
                handleUnexpectedError(t);
            }
        });

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        shakeErrors(errors);
    }
}
