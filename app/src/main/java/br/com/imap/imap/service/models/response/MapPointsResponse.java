package br.com.imap.imap.service.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

/**
 * Created by Samuel Ribeiro on 10/01/2017.
 */

public class MapPointsResponse extends BaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("highway")
    @Expose
    private String highway;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("latitude_graus")
    @Expose
    private Integer latitudeGraus;
    @SerializedName("latitude_minutes")
    @Expose
    private Integer latitudeMinutes;
    @SerializedName("latitude_seconds")
    @Expose
    private Double latitudeSeconds;
    @SerializedName("latitude_direction")
    @Expose
    private String latitudeDirection;
    @SerializedName("longitude_graus")
    @Expose
    private Integer longitudeGraus;
    @SerializedName("longitude_minutes")
    @Expose
    private Integer longitudeMinutes;
    @SerializedName("longitude_seconds")
    @Expose
    private Double longitudeSeconds;
    @SerializedName("longitude_direction")
    @Expose
    private String longitudeDirection;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("parking")
    @Expose
    private String parking;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getHighway() {
        return highway;
    }

    public void setHighway(String highway) {
        this.highway = highway;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getLatitudeGraus() {
        return latitudeGraus;
    }

    public void setLatitudeGraus(Integer latitudeGraus) {
        this.latitudeGraus = latitudeGraus;
    }

    public Integer getLatitudeMinutes() {
        return latitudeMinutes;
    }

    public void setLatitudeMinutes(Integer latitudeMinutes) {
        this.latitudeMinutes = latitudeMinutes;
    }

    public Double getLatitudeSeconds() {
        return latitudeSeconds;
    }

    public void setLatitudeSeconds(Double latitudeSeconds) {
        this.latitudeSeconds = latitudeSeconds;
    }

    public String getLatitudeDirection() {
        return latitudeDirection;
    }

    public void setLatitudeDirection(String latitudeDirection) {
        this.latitudeDirection = latitudeDirection;
    }

    public Integer getLongitudeGraus() {
        return longitudeGraus;
    }

    public void setLongitudeGraus(Integer longitudeGraus) {
        this.longitudeGraus = longitudeGraus;
    }

    public Integer getLongitudeMinutes() {
        return longitudeMinutes;
    }

    public void setLongitudeMinutes(Integer longitudeMinutes) {
        this.longitudeMinutes = longitudeMinutes;
    }

    public Double getLongitudeSeconds() {
        return longitudeSeconds;
    }

    public void setLongitudeSeconds(Double longitudeSeconds) {
        this.longitudeSeconds = longitudeSeconds;
    }

    public String getLongitudeDirection() {
        return longitudeDirection;
    }

    public void setLongitudeDirection(String longitudeDirection) {
        this.longitudeDirection = longitudeDirection;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
