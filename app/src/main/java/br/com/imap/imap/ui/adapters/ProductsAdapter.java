package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.ProductsResponse;
import br.com.imap.imap.utils.GlideUtils;

/**
 * Created by Samuel on 08/01/2017.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductHolder> {

    Context context;
    List<ProductsResponse> productsList;

    public ProductsAdapter(Context context, List<ProductsResponse> productsList) {
        this.context = context;
        this.productsList = productsList;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_premios, parent, false);
        return new ProductsAdapter.ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int position) {

        GlideUtils.loadImage(productsList.get(holder.getAdapterPosition()).getPicture().getUrl(), context, holder.ivProduto, R.drawable.ic_pdf);
        holder.tvProductTitle.setText(productsList.get(holder.getAdapterPosition()).getName());
        holder.tvPontos.setText(context.getString(R.string.pontos).replace("{x}", productsList.get(holder.getAdapterPosition()).getPoints().toString()));

    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivProduto;
        TextView tvProductTitle;
        TextView tvPontos;

        public ProductHolder(View itemView) {
            super(itemView);
            ivProduto = (ImageView) itemView.findViewById(R.id.iv_product_image);
            tvProductTitle = (TextView) itemView.findViewById(R.id.tv_product_title);
            tvPontos = (TextView) itemView.findViewById(R.id.tv_pontuacao);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
