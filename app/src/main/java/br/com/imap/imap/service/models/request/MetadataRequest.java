package br.com.imap.imap.service.models.request;

import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public class MetadataRequest extends BaseModel {

    @SerializedName("device_id")
    private String device_id;

    @SerializedName("device_os")
    private String device_os;

    public MetadataRequest(String device_id, String device_os) {
        this.device_id = device_id;
        this.device_os = device_os;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_os() {
        return device_os;
    }

    public void setDevice_os(String device_os) {
        this.device_os = "android";
    }
}
