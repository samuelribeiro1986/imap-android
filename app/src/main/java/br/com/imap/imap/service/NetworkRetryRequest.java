package br.com.imap.imap.service;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public class NetworkRetryRequest<T> {

    private Call<T> call;
    private Callback<T> callback;

    public NetworkRetryRequest(Call<T> tCall, Callback<T> tCallback) {
        call = tCall;
        callback = tCallback;
    }

    public void retry() {

        if (callback instanceof MyCallbackApi)
            ((MyCallbackApi) callback).before();

        call.clone().enqueue(callback);
    }
}
