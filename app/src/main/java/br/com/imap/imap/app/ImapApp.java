package br.com.imap.imap.app;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;

import org.androidannotations.annotations.EApplication;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import br.com.imap.imap.R;
import br.com.imap.imap.service.MyApiEndpointInterface;
import br.com.imap.imap.service.models.User;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Samuel Ribeiro on 01/12/2016.
 */

@EApplication
public class ImapApp extends Application {

    @Pref
    public static  Prefs_ prefs;
    private static Session session;
    private static MyApiEndpointInterface apiService = null;
    public boolean needToRefreshTrainings = false;
    private User currentUser;

    public static MyApiEndpointInterface getApiService() {
        if (apiService == null) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            Interceptor headerInterceptor = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder();

                    if (!ImapApp.prefs.accessToken().get().equals("")) {
                        requestBuilder.addHeader(Constants.HEADER.ACCESS_TOKEN, ImapApp.prefs.accessToken().get());
                        requestBuilder.addHeader(Constants.HEADER.CLIENT, ImapApp.prefs.client().get());
                        requestBuilder.addHeader(Constants.HEADER.UID, ImapApp.prefs.uid().get());
                    }
                    requestBuilder.addHeader(Constants.HEADER.CONTENT_TYPE, Constants.HEADER.APPLICATION_JSON);

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                }
            };

            Interceptor responseInterceptor = new Interceptor() {

                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    Log.w("Retrofit@Response", response.body().string());
                    return response;
                }
            };

            final OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(logInterceptor)
                    .addInterceptor(headerInterceptor)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.URL_BASE)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build();

            apiService = retrofit.create(MyApiEndpointInterface.class);
        }

        return apiService;
    }

    @Override
    public void onCreate() {
        Dexter.initialize(this);
        configFonts();
    }

    private void configFonts() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.fontAvenirBook))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public User getCurrentUser() {
        if (currentUser == null) {
            currentUser = (new Gson()).fromJson(prefs.currUser().get(), User.class);
        }
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
        ImapApp.prefs.currUser().put((new Gson().toJson(currentUser)));
        ImapApp.prefs.userId().put(currentUser.getId().toString());
    }

}
