package br.com.imap.imap.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.SplashResponse;
import br.com.imap.imap.ui.home.BaseActivity;
import br.com.imap.imap.ui.intro.LoginActivity_;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Samuel Ribeiro on 11/01/2017.
 */

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSecondSplash();
    }

    public void getSecondSplash() {
        ImapApp.getApiService().getSplashes().enqueue(new MyCallbackApi<List<SplashResponse>, BaseActivity>(this) {

            @Override
            public void onSuccess(Response<List<SplashResponse>> response) {
                try {
                    SecondSplashActivity_.intent(SplashActivity.this)
                            .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .extra(Constants.EXTRA.SPLASH, response.body().get(0).getImage().getUrl())
                            .start();
                    finish();
                } catch (Exception ex) {
                    LoginActivity_.intent(SplashActivity.this)
                            .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .extra(Constants.EXTRA.SPLASH, response.body().get(0).getImage().getUrl())
                            .start();
                    finish();
                }
            }

            @Override
            public void onUnexpectedError(Call<List<SplashResponse>> call, Throwable t) {
                Log.e(Constants.TAG, "onUnexpectedError: " + t.getMessage());
            }
        });
    }
}
