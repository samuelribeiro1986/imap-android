package br.com.imap.imap.ui.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.custom.RecyclerViewEmptySupport;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.RescuePointsResponse;
import br.com.imap.imap.ui.adapters.CityAdapter;
import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;

@EActivity(R.layout.activity_select_city)
public class SelectCityActivity extends BaseActivity implements RecyclerViewItemClick<RescuePointsResponse> {

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;

    @ViewById(R.id.empty_notif_results_view)
    View emptyView;

    @ViewById(R.id.rescues_list)
    RecyclerViewEmptySupport myList;

    @ViewById(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayoutManager lm;

    @Bean
    CityAdapter cityAdapter;

    @Extra(Constants.EXTRA.PRODUCT_ID)
    String productID;

    @Extra(Constants.EXTRA.PRODUCT_IMAGE)
    String productImage;

    @Extra(Constants.EXTRA.STATE)
    String state;

    private List<RescuePointsResponse> rescuePoints;

    private boolean isCallingApi = false;

    @AfterViews
    public void afterViews() {
        setupToolbar(getString(R.string.selecionar_unidade), true, false);
        populateCitiesList();
    }

    private void populateCitiesList() {
        setupPropertiesAdapter();

        if (rescuePoints != null && rescuePoints.size() > 0) {
            cityAdapter.setData(rescuePoints);
        } else {
            getRescuesPoints();
        }
    }

    private void setupPropertiesAdapter() {

        lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        myList.setLayoutManager(lm);
        cityAdapter.setOnItemClickListener(this);

        myList.setAdapter(cityAdapter);
        myList.setEmptyView(emptyView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRescuesPoints();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
    }

    private void getRescuesPoints() {

        ImapApp.getApiService().getRescueCities(state).enqueue(new MyCallbackApi<List<RescuePointsResponse>, BaseActivity>(this) {

            @Override
            public void after() {
                if (emptyView == null) return;
                isCallingApi = false;
                if (progressView != null && myList != null && swipeRefreshLayout != null) {
                    progressView.setVisibility(View.GONE);
                    myList.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void before() {
                if (emptyView == null) return;
                isCallingApi = true;
                emptyView.setVisibility(View.GONE);
                if (!swipeRefreshLayout.isRefreshing()) {
                    progressView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(Response<List<RescuePointsResponse>> response) {
                rescuePoints = response.body();
                cityAdapter.setData(rescuePoints);

            }

            @Override
            public void onUnexpectedError(Call<List<RescuePointsResponse>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClicked(View view, RescuePointsResponse item) {
        ReceiptActivity_.intent(this)
                .extra(Constants.EXTRA.RESCUE_POINTS, item)
                .extra(Constants.EXTRA.PRODUCT_ID, productID)
                .extra(Constants.EXTRA.PRODUCT_IMAGE, productImage)
                .start();
    }
}
