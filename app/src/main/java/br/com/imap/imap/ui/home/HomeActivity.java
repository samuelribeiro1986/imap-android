package br.com.imap.imap.ui.home;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import br.com.imap.imap.R;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.ui.adapters.NavigationMenuAdapter;
import br.com.imap.imap.ui.fragments.mapa.MapaFragment;
import br.com.imap.imap.ui.fragments.mapa.MapaFragment_;
import br.com.imap.imap.ui.fragments.perfil.PerfilFragment;
import br.com.imap.imap.ui.fragments.perfil.PerfilFragment_;
import br.com.imap.imap.ui.fragments.premios.PremioFragment;
import br.com.imap.imap.ui.fragments.premios.PremioFragment_;
import br.com.imap.imap.ui.fragments.ranking.RankingFragment;
import br.com.imap.imap.ui.fragments.ranking.RankingFragment_;
import br.com.imap.imap.ui.fragments.training.LessonsFragment;
import br.com.imap.imap.ui.fragments.training.TrainingFragment;
import br.com.imap.imap.ui.fragments.training.TrainingFragment_;
import br.com.imap.imap.utils.Utils;

@EActivity(R.layout.activity_home)
public class HomeActivity extends BaseActivity {

    public static final int STACKID_MAPA = 0;
    public static final int STACKID_TREINO = 1;
    public static final int STACKID_RANKING = 2;
    public static final int STACKID_PREMIOS = 3;
    public static final int STACKID_PERFIL = 4;

    @ViewById(R.id.drawer_layout)
    public static DrawerLayout drawerLayout;
    @ViewById(R.id.bottomBar)
    BottomBar mBottomBar;

    public Toolbar toolbar;
    public List<Stack> listStackFragment;
    protected boolean runningBackground = false;
    private TrainingFragment trainingFragment;
    private RankingFragment rankingFragment;
    private PremioFragment premioFragment;
    private MapaFragment mapaFragment;
    private PerfilFragment perfilFragment;
    private int STACKID_SELECT = STACKID_MAPA;
    private String navTitles[];
    private RecyclerView drawerRecycler;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private int lastTab;
    private View headerView;


    @AfterViews
    public void onInit() {
        initStackFragment();
        mBottomBar = (BottomBar) findViewById(R.id.bottomBar);
        setupToolbar("NAESTRADA", false, true);
        initFragments();
        setDrawerMenu();
        initBottomBar();
        Utils.checkAndRequestPermissions(getBaseContext(), this);
    }

    private void initFragments() {
        mapaFragment = new MapaFragment_();
        trainingFragment = new TrainingFragment_();
        rankingFragment = new RankingFragment_();
        premioFragment = new PremioFragment_();
        perfilFragment = new PerfilFragment_();
    }

    private void initBottomBar() {
        mBottomBar.setDefaultTabPosition(0);
        mBottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                if (tabId == R.id.btn_perfil) {
                    if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                        drawerLayout.closeDrawers();
                        selectedTab(lastTab);
                        mBottomBar.getCurrentTab().setSelected(false);
                        mBottomBar.selectTabAtPosition(lastTab);
                        mBottomBar.getTabAtPosition(lastTab).performClick();
                    } else {
                        drawerLayout.openDrawer(GravityCompat.END);
                        selectedTab(STACKID_PERFIL);
                    }
                }
            }
        });

        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                mBottomBar.getCurrentTab().setEnabled(false);
                switch (tabId) {
                    case R.id.btn_mapa:
                        selectedTab(STACKID_MAPA);
                        saveLastTab(STACKID_MAPA);
                        checkDrawerState();
                        break;
                    case R.id.btn_treino:
                        selectedTab(STACKID_TREINO);
                        saveLastTab(STACKID_TREINO);
                        checkDrawerState();
                        break;
                    case R.id.btn_ranking:
                        selectedTab(STACKID_RANKING);
                        saveLastTab(STACKID_RANKING);
                        checkDrawerState();
                        break;
                    case R.id.btn_premio:
                        selectedTab(STACKID_PREMIOS);
                        saveLastTab(STACKID_PREMIOS);
                        checkDrawerState();
                        break;
                    case R.id.btn_perfil:
                        drawerLayout.openDrawer(GravityCompat.END);
                        selectedTab(STACKID_PERFIL);
                        break;
                }
            }
        });
    }

    public void checkDrawerState() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawers();
        }
    }

    public void saveLastTab(int lastTab) {
        this.lastTab = lastTab;
    }

    private void selectedTab(int stackId) {
        STACKID_SELECT = stackId;
        switch (stackId) {
            case STACKID_MAPA:
                removeFragment();
                replaceFragment(mapaFragment);
                break;
            case STACKID_TREINO:
                removeFragment();
                replaceFragment(trainingFragment);
                break;
            case STACKID_RANKING:
                removeFragment();
                replaceFragment(rankingFragment);
                break;
            case STACKID_PREMIOS:
                removeFragment();
                replaceFragment(premioFragment);
                break;
            case STACKID_PERFIL:
                removeFragment();
                replaceFragment(perfilFragment);
                break;
        }
    }

    public void removeFragment() {
        if (getSupportFragmentManager().findFragmentById(R.id.content_fragment) != null)
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.content_fragment)).commitAllowingStateLoss();
    }

    public void replaceFragment(final Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.content_fragment, fragment);
        fragmentTransaction.commit();

        mBottomBar.getCurrentTab().setEnabled(true);
    }


    private void initStackFragment() {
        listStackFragment = new ArrayList<>();

        listStackFragment.add(new Stack<Fragment>());
        listStackFragment.add(new Stack<Fragment>());
        listStackFragment.add(new Stack<Fragment>());
        listStackFragment.add(new Stack<Fragment>());
        listStackFragment.add(new Stack<Fragment>());

        listStackFragment.get(STACKID_MAPA).push(mapaFragment);
        listStackFragment.get(STACKID_TREINO).push(LessonsFragment.newInstance());
        listStackFragment.get(STACKID_RANKING).push(RankingFragment.newInstance());
        listStackFragment.get(STACKID_PREMIOS).push(PremioFragment.newInstance());
        listStackFragment.get(STACKID_PERFIL).push(PremioFragment.newInstance());
    }

    private void setDrawerMenu() {
        //Drawer menu
        drawerRecycler = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        drawerRecycler.setLayoutManager(layoutManager);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerRecycler.setHasFixedSize(true);

        //Seta titulos and icones do Drawer Navigation
        navTitles = getResources().getStringArray(R.array.NavDrawerItens);
        adapter = new NavigationMenuAdapter(navTitles, this, app.getCurrentUser());
        drawerRecycler.setAdapter(adapter);
    }
}
