package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import br.com.imap.imap.custom.ViewWrapper;
import br.com.imap.imap.service.models.response.TrainingsResponse;
import br.com.imap.imap.ui.adapters.viewholders.FeedTrainingsViewHolder;
import br.com.imap.imap.ui.adapters.viewholders.FeedTrainingsViewHolder_;

/**
 * Created by Samuel Ribeiro on 04/01/2017.
 */

@EBean
public class FeedTrainingsAdapter extends RecyclerViewAdapterBase<TrainingsResponse, FeedTrainingsViewHolder> {

    @RootContext
    Context context;
    protected List<TrainingsResponse> listTrainings;

    @Override
    protected FeedTrainingsViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return FeedTrainingsViewHolder_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<FeedTrainingsViewHolder> holder, int position) {
        FeedTrainingsViewHolder view = holder.getView();

        final TrainingsResponse trainingsResponse = listTrainings.get(position);

        view.bind(trainingsResponse);

    }
}
