package br.com.imap.imap.service.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

/**
 * Created by Samuel Ribeiro on 10/01/2017.
 */

public class Content extends BaseModel {

    @SerializedName("training_id")
    @Expose
    private Integer trainingId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;

    public Integer getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(Integer trainingId) {
        this.trainingId = trainingId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}
