package br.com.imap.imap.ui.fragments.ranking;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.ui.adapters.RankingViewPagerAdapter;

@EFragment(R.layout.fragment_ranking)
public class RankingFragment extends Fragment {

    @ViewById(R.id.profile_view_pager)
    ViewPager profileViewPager;

    @ViewById(R.id.trainings_tabs)
    TabLayout profileTabLayout;

    @App
    ImapApp app;

    public static RankingFragment newInstance() {
        Bundle args = new Bundle();
        RankingFragment fragment = new RankingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    public void afterView() {

        setupViewPager(profileViewPager);
        profileTabLayout.setupWithViewPager(profileViewPager);
    }

    public void setupViewPager(ViewPager viewPager) {
        RankingViewPagerAdapter rankingViewPagerAdapter = new RankingViewPagerAdapter(getChildFragmentManager());
        rankingViewPagerAdapter.addFragment(RankingCategoryFragment_.builder().rankingFragmentType(Constants.MY_RANKING).build(), getString(R.string.my_ranking));
        rankingViewPagerAdapter.addFragment(RankingCategoryFragment_.builder().rankingFragmentType(Constants.TOP_TEN).build(), getString(R.string.top_ten));
        rankingViewPagerAdapter.addFragment(RankingCategoryFragment_.builder().rankingFragmentType(Constants.MY_DRIVERS).build(), getString(R.string.my_drivers));
        viewPager.setAdapter(rankingViewPagerAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getChildFragmentManager() != null && getChildFragmentManager().getFragments() != null) {
            for (Fragment fragment : getChildFragmentManager().getFragments()) {
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

}
