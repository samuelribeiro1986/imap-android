package br.com.imap.imap.app;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public class Constants {
    public static final int REQUEST_GPS = 9000;
    public static final int REQUEST_CAMERA = 9003;
    public static final int REQUEST_GALLERY = 9004;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9010;
    private static final boolean isProdution = false;
    public static final String URL_BASE = isProdution ? "url_producao" :
            "http://imap.ioasys.com.br:8097/api/v1/";
    public static final String HOST = "http://imap.ioasys.com.br:8097";
    public static final String TAG = "br.com.imap.imap";
    public static final String VIDEOS = "videos";
    public static final String DOCUMENTS = "documents";
    public static final String MY_RANKING = "my_ranking";
    public static final String TOP_TEN = "top_ten";
    public static final String MY_DRIVERS = "my_drivers";


    public static final class API {

        public static class Account {
            public static final String OMNIAUTH = "users/omniauth/facebook";
            public static final String SIGNIN = "authentication/sessions/";
            public static final String SIGNOUT = "users/auth/sign_out/";
            public static final String UPDATE_PROVIDER = "users/{id}/update_provider";
            public static final String CREATE = "users/auth/";
            public static final String UPDATE = CREATE;
            public static final String SHOW = "users/{id}";
            public static final String REGISTER_DEVICE = "users/{id}/register_device";
            public static final String NOTIFICATION = "users/{user_id}/notifications";

        }

        public static class Trainings {
            public static final String INDEX = "trainings";
        }

        public static class Ranking {
            public static final String TOP_TEN = "top_ranking";
            public static final String MY_RANK = "user_ranking";
        }

        public static class Products {
            public static final String INDEX = "products";
        }

        public static class Rescues {
            public static final String INDEX = "rescues";
            public static final String CREATE = "rescues";
        }

        public static class RescuePoints {
            public static final String INDEX = "rescue_points";
        }

        public static class Notifications {
            public static final String INDEX = "notifications";
        }

        public static class MapPoints {
            public static final String INDEX = "map_points";
        }

        public static class Ocurrence {
            public static final String CREATE = "ocurrences";
        }

        public static class Splash {
            public static final String INDEX = "splashes";
        }
    }

    public class HEADER {
        public static final String ENCODING = "Accept-Encoding";
        public static final String ENCODING_TYPE = "gzip";
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String ACCESS_TOKEN = "access-token";
        public static final String CLIENT = "client";
        public static final String UID = "uid";
        static final String APPLICATION_JSON = "application/json";
    }

    public class EXTRA {
        public static final String OPEN_NOTIFICATION = "open_notification";
        public static final String VIDEO_ID = "video_id";
        public static final String QUESTIONS = "questions";
        public static final String SPLASH = "splash";
        public static final String STATE = "state";
        public static final String RESCUE_POINTS = "rescue_points";
        public static final String USER = "user";
        public static final String USER_ID = "user_id";
        public static final String SELLER_ID = "seller_id";
        public static final String SELLER = "seller";
        public static final String IS_CURR_USER = "isCurrUser";
        public static final String FACEBOOK_DATA = "facebook_data";
        public static final String FIRST_IMAGE = "first_image";
        public static final String PRODUCT = "product";
        public static final String PRODUCT_ID = "product_id";
        public static final String PRODUCT_IMAGE = "product_image";
        public static final String PURCHASE = "purchase";
        public static final String CAUSE = "cause";
        public static final String CAUSE_ID = "cause_id";
        public static final String CAUSE_PRICE = "cause_price";
        public static final String CAUSE_SCREEN_TYPE = "cause_screen_type";
        public static final String IMAGE_TO_CROP = "image_to_crop";
        public static final String IMAGE_TO_ZOOM = "image_to_zoom";
        public static final String BUNDLE_IMAGES_PATH = "image_path";
        public static final String DEVICE_OS = "android";
        public static final String PROFILE_FRAGMENT_TYPE = "profile_fragment_type";


        public static final String UPDATE_HOME_LIST = "UPDATE_HOME_list";
    }
}
