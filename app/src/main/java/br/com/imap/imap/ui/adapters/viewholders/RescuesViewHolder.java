package br.com.imap.imap.ui.adapters.viewholders;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.enums.StatusEnum;
import br.com.imap.imap.service.models.response.Rescues;
import br.com.imap.imap.utils.GlideUtils;

/**
 * Created by Samuel Ribeiro on 09/01/2017.
 */
@EViewGroup(R.layout.adapter_premios_resgatados)
public class RescuesViewHolder extends RelativeLayout{

    Context mContext;
    @ViewById(R.id.iv_product_image)
    ImageView ivProductImage;
    @ViewById(R.id.tv_product_name)
    TextView tvProductName;
    @ViewById(R.id.tv_product_status)
    TextView tvProductStatus;
    @ViewById(R.id.tv_produt_pontuacao)
    TextView tvProdutPontuacao;

    public RescuesViewHolder(Context context) {
        super(context);
        this.mContext = context;
    }

    public void bind (Rescues rescues){
        GlideUtils.loadImage(rescues.getProduct().getPicture().getUrl(), mContext, ivProductImage, R.drawable.user_image_place_holder);
        tvProductName.setText(rescues.getProduct().getName());
        tvProductStatus.setText(StatusEnum.getNameByValue(rescues.getStatus()));
        tvProdutPontuacao.setText(mContext.getString(R.string.pontos).replace("{x}", rescues.getProduct().getPoints().toString()));
    }
}
