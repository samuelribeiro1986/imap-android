package br.com.imap.imap.service;

import java.io.IOException;

import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public abstract class MyCallbackApi<T, A extends BaseActivity> implements Callback<T> {

    A activity;

    public MyCallbackApi(A context) {
        this.activity = context;
        before();
    }


    public abstract void onSuccess(Response<T> response);

    public abstract void onUnexpectedError(Call<T> call, Throwable t);


    public void before() {

    }

    public void after() {

    }

    public void onApiError(Response<T> response) {
        activity.handleAPIErrors(response.errorBody());

    }

    public void onNetworkError(Call<T> call) {
        activity.onNetworkError(new NetworkRetryRequest<>(call, this));
    }


    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (activity == null) {
            return;
        }
        after();
        activity.updateHeaderData(response.headers());


        if (response.code() == 404) {
            activity.handleNotFoundError(response);
            return;
        }


        if (response.code() == 401) {
            activity.handleUnAuthorizedRequest(response);
            return;
        }


        if (!response.isSuccessful() || response.code() == 422) {
            onApiError(response);
            return;
        }

        onSuccess(response);

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (activity == null) {
            return;
        }
        after();

        if (t instanceof IOException) {
            onNetworkError(call);
            return;
        }

        onUnexpectedError(call, t);
    }

}