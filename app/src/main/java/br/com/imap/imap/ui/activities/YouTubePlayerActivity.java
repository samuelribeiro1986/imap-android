package br.com.imap.imap.ui.activities;

import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.iid.FirebaseInstanceId;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import java.util.ArrayList;
import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.enums.AnswerEnum;
import br.com.imap.imap.service.YouTubeActivityCallbackApi;
import br.com.imap.imap.service.models.User;
import br.com.imap.imap.service.models.request.LoginRequest;
import br.com.imap.imap.service.models.request.MetadataRequest;
import br.com.imap.imap.service.models.response.Option;
import br.com.imap.imap.service.models.response.Questions;
import br.com.imap.imap.ui.home.HomeActivity_;
import br.com.imap.imap.utils.DialogUtils;
import retrofit2.Call;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

@EActivity(R.layout.activity_you_tube_player)
public class YouTubePlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final String API_KEY = "";
    @Extra(Constants.EXTRA.VIDEO_ID)
    String videoID;
    @Extra(Constants.EXTRA.QUESTIONS)
    Questions questions;
    ImapApp app;
    private YouTubePlayer youTubePlayer;
    private List<Option> optionsList;
    private String[] optionsListString = new String[4];
    private int totalAttempts;

    private long startTime;

    private long endTime;
    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onPlaying() {

        }

        @Override
        public void onPaused() {

        }

        @Override
        public void onStopped() {

        }

        @Override
        public void onBuffering(boolean b) {

        }

        @Override
        public void onSeekTo(int i) {
            Log.i(Constants.TAG, "onSeekTo: " + i);
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onLoaded(String s) {

        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {

            DialogUtils.ChoiceDialog(YouTubePlayerActivity.this, optionsListString, questions.getContent(), new MaterialDialog.ListCallbackSingleChoice() {

                @Override
                public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                    checkResponse(which);
                    dialog.dismiss();
                    return false;
                }
            });
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }
    };

    @AfterViews
    public void afterViews() {
        optionsList = new ArrayList<>();

        getOptionsList();

        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
        youTubePlayerView.initialize(getString(R.string.api_key), this);

        callService();
    }

    public boolean checkQuestionAlreadyAnswered() {
        if (questions != null) {
            if (totalAttempts > 2 && questions.getUserAnswers().getAnswerCorrect()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {

        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);

        this.youTubePlayer = youTubePlayer;

        if (!wasRestored && totalAttempts <= 2 && !checkQuestionAlreadyAnswered()) {
            youTubePlayer.loadVideo(videoID);
            startTime = SystemClock.elapsedRealtime();
        } else {
            showOverAttemptsDialog(getCorrectAnswer());
            startTime = SystemClock.elapsedRealtime();
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failure to Initialize!", Toast.LENGTH_LONG).show();
    }

    private void getOptionsList() {
        totalAttempts = questions.getUserAnswers().getAnswersCount();
        if (questions != null) {
            for (int i = 0; i < questions.getOptions().size(); i++) {
                optionsListString[i] = questions.getOptions().get(i).getContent();
                optionsList.add(questions.getOptions().get(i));
            }
        }
    }

    private void checkResponse(int response) {
        int correctAnswer;
        if (questions != null) {
            for (int i = 0; i < questions.getOptions().size(); i++) {
                if (questions.getOptions().get(i).getCorrect()) {
                    correctAnswer = i;
                    if (correctAnswer == response) {
                        endTime = SystemClock.elapsedRealtime();
                        calculateElapsedTime();
                        showCorrectAnswerDialog(questions.getPoints().toString());
                    } else {
                        if (totalAttempts <= 2) {
                            showWrongAnswerDilaog();
                            totalAttempts++;
                        } else {
                            showOverAttemptsDialog(getCorrectAnswer());
                        }
                    }
                }
            }
        }
    }

    private String getCorrectAnswer() {
        String answer = "";
        if (optionsList != null && optionsList.size() > 0) {
            for (int i = 0; i < optionsList.size(); i++) {
                if (optionsList.get(i).getCorrect()) {
                    answer = AnswerEnum.getNameById(i);
                }
            }
        }
        return answer;
    }

    private int calculateElapsedTime() {
        double elapsedSeconds = endTime - startTime;
        return (int) elapsedSeconds;
    }

    public void showCorrectAnswerDialog(String pontos) {
        DialogUtils.createCustomDialogWithoutButtons(YouTubePlayerActivity.this, R.layout.dialog_correct_answer, pontos);
    }

    public MaterialDialog showWrongAnswerDilaog() {

        final MaterialDialog dialog = new MaterialDialog.Builder(YouTubePlayerActivity.this)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.START)
                .widgetColorRes(R.color.colorPrimaryDark)
                .cancelable(true)
                .customView(R.layout.dialog_wrong_answer, false)
                .show();

        Button btnAssistirNovamente = (Button) dialog.findViewById(R.id.btn_assistir);
        btnAssistirNovamente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (youTubePlayer != null && videoID != null)
                    youTubePlayer.loadVideo(videoID);

            }
        });

        Button btnTentarDepois = (Button) dialog.findViewById(R.id.btn_tentar);
        btnTentarDepois.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity_.intent(YouTubePlayerActivity.this)
                        .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .start();
            }
        });

        return dialog;
    }

    public MaterialDialog showOverAttemptsDialog(String answer) {
        final MaterialDialog dialog = new MaterialDialog.Builder(YouTubePlayerActivity.this)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.START)
                .widgetColorRes(R.color.colorPrimaryDark)
                .cancelable(true)
                .customView(R.layout.dialog_over_attempt, false)
                .show();

        TextView pontos = (TextView) dialog.findViewById(R.id.tv_correct_answer);
        pontos.setText(getString(R.string.resposta).replace("{x}", answer));

        Button btnAssistirNovamente = (Button) dialog.findViewById(R.id.btn_ok);
        btnAssistirNovamente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (youTubePlayer != null && videoID != null)
                    youTubePlayer.loadVideo(videoID);

            }
        });

        return dialog;
    }

    public void callService() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        ImapApp.getApiService().signin(new LoginRequest("leandro86marques@gmail.com", "123456", new MetadataRequest(refreshedToken, Constants.EXTRA.DEVICE_OS)))
                .enqueue(new YouTubeActivityCallbackApi<User, YouTubePlayerActivity>(this) {

                    @Override
                    public void after() {
                        super.after();
                    }

                    @Override
                    public void before() {
                        super.before();
                    }

                    @Override
                    public void onResponse(Call call, Response response) {
                        Log.i(TAG, "onResponse: " + response);
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.i(TAG, "onFailure: " + t);
                    }

                    @Override
                    public void onSuccess(Response<User> response) {
                        Log.i(Constants.TAG, "onSuccess: " + response);
                    }

                    @Override
                    public void onUnexpectedError(Call call, Throwable t) {
                        Log.e(Constants.TAG, "onUnexpectedError: " + t.getMessage());
                    }
                });
    }
}
