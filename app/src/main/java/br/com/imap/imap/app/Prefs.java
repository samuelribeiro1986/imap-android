package br.com.imap.imap.app;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface Prefs {
    @DefaultString("")
    String accessToken();

    @DefaultString("")
    String client();

    @DefaultString("")
    String userId();

    @DefaultString("")
    String uid();

    @DefaultString("")
    String currUser();

    @DefaultBoolean(false)
    boolean tutorialShown();

    @DefaultBoolean(false)
    boolean cameraTutorialShown();

    @DefaultString("")
    String gcmToken();

    boolean gcmTokenSavedInServer();

    String filters();
}
