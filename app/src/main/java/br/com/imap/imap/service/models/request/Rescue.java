package br.com.imap.imap.service.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

public class Rescue extends BaseModel{

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("rescue_point_id")
    @Expose
    private String rescuePointId;

    public Rescue(String productId, String rescuePointId) {
        this.productId = productId;
        this.rescuePointId = rescuePointId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getRescuePointId() {
        return rescuePointId;
    }

    public void setRescuePointId(String rescuePointId) {
        this.rescuePointId = rescuePointId;
    }

}