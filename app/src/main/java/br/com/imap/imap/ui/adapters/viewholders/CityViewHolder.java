package br.com.imap.imap.ui.adapters.viewholders;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.lang3.StringUtils;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.RescuePointsResponse;

/**
 * Created by Samuel Ribeiro on 13/01/2017.
 */
@EViewGroup(R.layout.adapter_city)
public class CityViewHolder extends RelativeLayout {

    Context context;

    @ViewById(R.id.tv_city_name)
    TextView tvCityName;

    public CityViewHolder(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(RescuePointsResponse rescuePoints) {
        tvCityName.setText(StringUtils.capitalize(rescuePoints.getCity().toLowerCase()));
    }
}
