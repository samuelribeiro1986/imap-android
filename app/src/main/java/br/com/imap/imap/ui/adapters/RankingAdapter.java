package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.RankingResponse;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Samuel on 08/01/2017.
 */

public class RankingAdapter extends RecyclerView.Adapter<RankingAdapter.RankingHolder> {

    Context context;
    List<RankingResponse> rankingList;

    public RankingAdapter(Context context, List<RankingResponse> rankingList) {
        this.context = context;
        this.rankingList = rankingList;
    }

    @Override
    public RankingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ranking, parent, false);
        return new RankingAdapter.RankingHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RankingHolder holder, int position) {
        try {
            holder.tvRankingPosition.setText(rankingList.get(holder.getAdapterPosition()).getPosition().toString());
        } catch (Exception ex) {

        }
        try {
            Glide.with(context).load(rankingList.get(holder.getAdapterPosition()).getAvatar()).placeholder(R.drawable.user_image_place_holder).bitmapTransform(new CropCircleTransformation(context)).into(holder.ivUserImage);
        } catch (Exception ex) {

        }
        holder.tvUserName.setText(rankingList.get(holder.getAdapterPosition()).getName());
        holder.tvUserScore.setText(context.getString(R.string.pontos_acumulados).replace("{x}", rankingList.get(holder.getAdapterPosition()).getTotalPoints().toString()));
    }

    @Override
    public int getItemCount() {
        return rankingList.size();
    }

    public class RankingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivUserImage;
        TextView tvRankingPosition;
        TextView tvUserName;
        TextView tvUserScore;
        LinearLayout llItemDocument;

        public RankingHolder(View itemView) {
            super(itemView);
            ivUserImage = (ImageView) itemView.findViewById(R.id.iv_user_image);
            tvRankingPosition = (TextView) itemView.findViewById(R.id.tv_rank_position);
            tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
            tvUserScore = (TextView) itemView.findViewById(R.id.tv_user_score);
            llItemDocument = (LinearLayout) itemView.findViewById(R.id.ll_item_ranking);
        }

        @Override
        public void onClick(View view) {


        }
    }
}
