package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.imap.imap.custom.ViewWrapper;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.models.response.Rescues;
import br.com.imap.imap.ui.adapters.viewholders.RescuesViewHolder;
import br.com.imap.imap.ui.adapters.viewholders.RescuesViewHolder_;

/**
 * Created by Samuel Ribeiro on 09/01/2017.
 */
@EBean
public class RescuesAdapter extends RecyclerViewAdapterBase<Rescues, RescuesViewHolder> {

    @RootContext
    Context context;
    private RecyclerViewItemClick<Rescues> listener;

    public void setOnItemClickListener(@NonNull RecyclerViewItemClick<Rescues> listener) {
        this.listener = listener;
    }

    @Override
    protected RescuesViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return RescuesViewHolder_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<RescuesViewHolder> holder, int position) {
        RescuesViewHolder view = holder.getView();
        final Rescues rescues = items.get(position);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        view.bind(rescues);
    }
}
