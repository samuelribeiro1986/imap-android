package br.com.imap.imap.ui.fragments.premios;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.custom.RecyclerViewEmptySupport;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.ProductsResponse;
import br.com.imap.imap.ui.activities.PrizeRescueActivity_;
import br.com.imap.imap.ui.adapters.PrizeAdapter;
import br.com.imap.imap.ui.adapters.ProductsAdapter;
import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;

@EFragment(R.layout.fragment_premio)
public class PremioFragment extends Fragment implements RecyclerViewItemClick<ProductsResponse> {

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;
    @ViewById(R.id.recyclerView_products)
    RecyclerViewEmptySupport recyclerViewProducts;
    @ViewById(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @ViewById(R.id.empty_feed_results_view)
    View emptyView;

    @App
    ImapApp app;

    @Bean
    PrizeAdapter prizeAdapter;

    ProductsAdapter productsAdapter;

    private List<ProductsResponse> productsResponses;
    private boolean loading = true;
    private boolean isCallingApi = false;
    private int currPage = 1;
    private boolean morePages = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;


    GridLayoutManager glm;

    public static PremioFragment newInstance() {
        Bundle args = new Bundle();
        PremioFragment fragment = new PremioFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    public void afterVies() {
        populateProductsFeed();
    }

    private void populateProductsFeed() {
        setupPropertiesAdapter();
        if (productsResponses != null && productsResponses.size() > 0) {
            if (app.needToRefreshTrainings) {
                app.needToRefreshTrainings = false;
                getFeed(1);
            } else {
                prizeAdapter.setData(productsResponses);
            }
        } else {
            getFeed(1);
        }
    }

    private void setAdapter(List<ProductsResponse> productsResponses) {
        productsAdapter = new ProductsAdapter(getActivity(), productsResponses);
        recyclerViewProducts.setAdapter(productsAdapter);
        recyclerViewProducts.setHasFixedSize(true);
    }

    private void setupPropertiesAdapter() {

        glm = new GridLayoutManager(getActivity(), 3);
        recyclerViewProducts.setLayoutManager(glm);
        prizeAdapter.setOnClickListener(this);

        recyclerViewProducts.setAdapter(prizeAdapter);
        recyclerViewProducts.setEmptyView(emptyView);


        recyclerViewProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = glm.getChildCount();
                    totalItemCount = glm.getItemCount();
                    pastVisibleItems = glm.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            if (morePages && !isCallingApi) {
                                currPage++;
                                getFeed(currPage);
                            }
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currPage = 1;
                morePages = true;
                getFeed(currPage);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);


    }


    private void getFeed(final int pageNumber) {

        ImapApp.getApiService().getListProducts(pageNumber).enqueue(new MyCallbackApi<List<ProductsResponse>, BaseActivity>(((BaseActivity) getActivity())) {

            @Override
            public void after() {
                if (emptyView == null) return;
                isCallingApi = false;
                if (progressView != null && recyclerViewProducts != null && swipeRefreshLayout != null) {
                    progressView.setVisibility(View.GONE);
                    recyclerViewProducts.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void before() {
                if (emptyView == null) return;
                isCallingApi = true;
                emptyView.setVisibility(View.GONE);
                if (!swipeRefreshLayout.isRefreshing()) {
                    progressView.setVisibility(View.VISIBLE);
                }
                if (pageNumber == 1) {
                    recyclerViewProducts.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(Response<List<ProductsResponse>> response) {
                if (getActivity() == null) return;
                productsResponses = response.body();
                if (productsResponses.size() == 0) {
                    morePages = false;
                }
                if (pageNumber == 1) {
                    prizeAdapter.setData(productsResponses);
                } else {
                    prizeAdapter.addData(productsResponses);
                }
            }

            @Override
            public void onUnexpectedError(Call<List<ProductsResponse>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClicked(View view, ProductsResponse item) {
        PrizeRescueActivity_.intent(getContext()).extra(Constants.EXTRA.PRODUCT, item).start();
    }
}
