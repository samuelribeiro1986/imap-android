//package br.com.imap.imap.ui;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.support.v4.widget.SwipeRefreshLayout;
//import android.support.v7.widget.LinearLayoutManager;
//import android.view.View;
//
//import com.github.rahatarmanahmed.cpv.CircularProgressView;
//import com.google.android.youtube.player.YouTubePlayer;
//
//import org.androidannotations.annotations.AfterViews;
//import org.androidannotations.annotations.Bean;
//import org.androidannotations.annotations.EActivity;
//import org.androidannotations.annotations.ViewById;
//
//import java.util.List;
//
//import br.com.imap.imap.R;
//import br.com.imap.imap.custom.RecyclerViewEmptySupport;
//import br.com.imap.imap.service.models.response.TrainingsResponse;
//import br.com.imap.imap.ui.adapters.FeedTrainingsAdapter;
//import br.com.imap.imap.ui.fragments.training.LessonsFragment;
//import br.com.imap.imap.ui.home.BaseActivity;
//
//@EActivity
//public class TrainingActivity extends BaseActivity implements YouTubePlayer.OnInitializedListener {
//
//    @ViewById(R.id.progress_view)
//    CircularProgressView progressView;
//
//    @ViewById(R.id.empty_feed_results_view)
//    View emptyView;
//
//    @ViewById(R.id.my_list)
//    RecyclerViewEmptySupport myList;
//
//    @ViewById(R.id.swipeRefreshLayout)
//    SwipeRefreshLayout swipeRefreshLayout;
//
//    @Bean
//    FeedTrainingsAdapter feedAdapter;
//    int pastVisibleItems, visibleItemCount, totalItemCount;
//    LinearLayoutManager lm;
//    private List<TrainingsResponse> feedResponses;
//    private boolean loading = true;
//    private boolean isCallingApi = false;
//
//    private int currPage = 1;
//    private boolean morePages = true;
//    public static LessonsFragment newInstance() {
//        Bundle args = new Bundle();
//        LessonsFragment fragment = new LessonsFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @AfterViews
//    void afterView(){
//        populateFeed();
//    }
//
//    private void populateFeed() {
//        setupPropertiesAdapter();
//    }
//
//    private void setupPropertiesAdapter() {
//    }
//}
