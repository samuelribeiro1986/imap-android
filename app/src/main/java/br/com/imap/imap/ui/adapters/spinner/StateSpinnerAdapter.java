package br.com.imap.imap.ui.adapters.spinner;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

import br.com.imap.imap.service.models.response.RescuePointsResponse;

/**
 * Created by Samuel Ribeiro on 12/01/2017.
 */

public class StateSpinnerAdapter extends ArrayAdapter<RescuePointsResponse> {

    public StateSpinnerAdapter(Context context, int resource, List<RescuePointsResponse> objects) {
        super(context, resource, objects);
    }

    public int getStatePosition(int id) {
        for (int i = 0; i < getCount(); i++) {
            RescuePointsResponse item = getItem(i);
            if (item != null && item.getId().equals(id)) return i;
        }

        return 0;
    }
}
