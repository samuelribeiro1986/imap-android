package br.com.imap.imap.ui.adapters.viewholders;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.ProductsResponse;
import br.com.imap.imap.utils.GlideUtils;

/**
 * Created by Samuel Ribeiro on 09/01/2017.
 */

@EViewGroup(R.layout.adapter_premios)
public class PrizesViewHolder extends LinearLayout{

    Context mContext;
    @ViewById(R.id.iv_product_image)
    ImageView ivProduto;
    @ViewById(R.id.tv_product_title)
    TextView tvProductTitle;
    @ViewById(R.id.tv_pontuacao)
    TextView tvPontos;

    public PrizesViewHolder(Context context) {
        super(context);
        this.mContext = context;
    }

    public void bind(ProductsResponse product){
        GlideUtils.loadImage(product.getPicture().getUrl(), mContext, ivProduto, R.drawable.product_place_holder);
        tvProductTitle.setText(product.getName());
        tvPontos.setText(mContext.getString(R.string.pontos).replace("{x}", product.getPoints().toString()));

    }
}
