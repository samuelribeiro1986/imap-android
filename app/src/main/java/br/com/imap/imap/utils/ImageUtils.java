package br.com.imap.imap.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.imap.imap.app.Constants;
import br.com.imap.imap.ui.home.BaseActivity;

/**
 * Created by Samuel Ribeiro on 11/01/2017.
 */

public class ImageUtils {

    private static final String PHOTO_SAVE_DIRECTORY = "oquevocequer";

    public static void pickPhotoFromGallery(Context context) {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            ((BaseActivity) context).startActivityForResult(intent, Constants.REQUEST_GALLERY);
        }
    }

    public static Uri openCameraIntent(Activity activity) {
        Uri imageUri;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
        String fileName = "IMG_" + sdf.format(new Date()) + ".jpg";
        File myDirectory = new File(Environment.getExternalStorageDirectory() + "/" + PHOTO_SAVE_DIRECTORY + "/");
        boolean isDirectoryCreated = myDirectory.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated = myDirectory.mkdirs();
        }
        if (!isDirectoryCreated) return null;
        File file = new File(myDirectory, fileName);
        imageUri = Uri.fromFile(file);
        Intent launchIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        launchIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        launchIntent.putExtra("return-data", true);
        activity.startActivityForResult(launchIntent, Constants.REQUEST_CAMERA);
        return imageUri;
    }
}
