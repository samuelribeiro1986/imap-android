package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.imap.imap.custom.ViewWrapper;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.models.response.ProductsResponse;
import br.com.imap.imap.ui.adapters.viewholders.PrizesViewHolder;
import br.com.imap.imap.ui.adapters.viewholders.PrizesViewHolder_;

/**
 * Created by Samuel Ribeiro on 09/01/2017.
 */
@EBean
public class PrizeAdapter extends RecyclerViewAdapterBase<ProductsResponse, PrizesViewHolder> {

    @RootContext
    Context context;
    private RecyclerViewItemClick<ProductsResponse> listener;

    public void setOnClickListener(@NonNull RecyclerViewItemClick<ProductsResponse> listener) {
        this.listener = listener;
    }

    @Override
    protected PrizesViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return PrizesViewHolder_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<PrizesViewHolder> holder, int position) {
        PrizesViewHolder view = holder.getView();
        final ProductsResponse productsResponse = items.get(position);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClicked(view, productsResponse);
            }
        });
        view.bind(productsResponse);

    }
}
