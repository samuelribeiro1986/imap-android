package br.com.imap.imap.ui.fragments.ranking;


import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.RankingResponse;
import br.com.imap.imap.ui.adapters.RankingAdapter;
import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;

@EFragment(R.layout.fragment_ranking_category)
public class RankingCategoryFragment extends Fragment {

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;

    @ViewById(R.id.empty_feed_results_view)
    View emptyView;

    @ViewById(R.id.my_list)
    RecyclerView myList;

    @ViewById(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @App
    ImapApp app;

    @FragmentArg(Constants.EXTRA.PROFILE_FRAGMENT_TYPE)
    String rankingFragmentType;

    private LinearLayoutManager linearLayoutManager;

    private RankingAdapter adapter;

    private List<RankingResponse> rankingList;

    @AfterViews
    public void afterViews() {
        manageTabsContent();
    }

    private void manageTabsContent() {
        if (rankingFragmentType.equals(Constants.MY_RANKING)) {
            getMyTopRanking();
        } else if (rankingFragmentType.equals(Constants.TOP_TEN)) {
            getTopTenRanking();
        } else if (rankingFragmentType.equals(Constants.MY_DRIVERS)) {
            getTopTenRanking();
        }
    }

    private void setAdapter(List<RankingResponse> rankingList) {
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new RankingAdapter(getActivity(), rankingList);
        myList.setLayoutManager(linearLayoutManager);
        myList.setAdapter(adapter);
        myList.setHasFixedSize(true);
    }

    public void getTopTenRanking() {
        ImapApp.getApiService().getTopTenRank().enqueue(new MyCallbackApi<List<RankingResponse>, BaseActivity>(((BaseActivity) getActivity())) {

            @Override
            public void after() {
                if (emptyView == null) return;
                if (progressView != null && myList != null && swipeRefreshLayout != null) {
                    progressView.setVisibility(View.GONE);
                    myList.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void before() {
                if (emptyView == null) return;
                if (!swipeRefreshLayout.isRefreshing()) {
                    progressView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(Response<List<RankingResponse>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    setAdapter(response.body());
                }
            }

            @Override
            public void onUnexpectedError(Call<List<RankingResponse>> call, Throwable t) {

            }
        });
    }

    public void getMyTopRanking() {
        ImapApp.getApiService().getMyRank().enqueue(new MyCallbackApi<List<RankingResponse>, BaseActivity>(((BaseActivity) getActivity())) {


            @Override
            public void onSuccess(Response<List<RankingResponse>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    setAdapter(response.body());
                }
            }

            @Override
            public void onUnexpectedError(Call<List<RankingResponse>> call, Throwable t) {

            }
        });
    }
}
