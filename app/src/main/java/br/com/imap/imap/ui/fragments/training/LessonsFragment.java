package br.com.imap.imap.ui.fragments.training;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.Document;
import br.com.imap.imap.service.models.response.TrainingsResponse;
import br.com.imap.imap.ui.adapters.DocumentAdapter;
import br.com.imap.imap.ui.adapters.FeedTrainingsAdapter;
import br.com.imap.imap.ui.adapters.RecyclerAdapter;
import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;


@EFragment(R.layout.fragment_lessons)
public class LessonsFragment extends Fragment implements RecyclerViewItemClick<Document> {
    public boolean needToRefreshTrainings = false;

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;

    @ViewById(R.id.recyclerView_documents)
    RecyclerView documentsList;

    @ViewById(R.id.empty_feed_results_view)
    View emptyView;

    @ViewById(R.id.my_list)
    RecyclerView myList;
    //    RecyclerView myList;

    @ViewById(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @ViewById(R.id.recycler_main)
    LinearLayout recyclerMain;

    @App
    ImapApp app;

    @Bean
    FeedTrainingsAdapter feedAdapter;

    @FragmentArg(Constants.EXTRA.PROFILE_FRAGMENT_TYPE)
    String trainingsFragmentType;

    LinearLayoutManager lm;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    private List<TrainingsResponse> trainingsResponses;
    private HashMap<Integer, String> hashMapDownloads;
    private List<Document> documents;
    private boolean loading = true;
    private boolean isCallingApi = false;
    private int currPage = 1;
    private boolean morePages = true;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerAdapter adapter;
    private DocumentAdapter documentAdapter;

    public static LessonsFragment newInstance() {
        Bundle args = new Bundle();
        LessonsFragment fragment = new LessonsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    void afterView() {
        manageTabsContent();
        hashMapDownloads = new HashMap<>();
    }

    private void getDocumentsList(List<TrainingsResponse> response) {
        documents = new ArrayList<>();
        if (response != null) {
            for (int i = 0; i < response.size(); i++) {
                for (int j = 0; j < response.get(j).getDocuments().size(); j++) {
                    documents.add(response.get(i).getDocuments().get(j));
                }
            }
        }
    }

    private void manageTabsContent() {
        if (trainingsFragmentType.equals(Constants.VIDEOS)) {
            documentsList.setVisibility(View.GONE);
            recyclerMain.setVisibility(View.VISIBLE);
            populateTrainingFeed();
        } else if (trainingsFragmentType.equals(Constants.DOCUMENTS)) {
            documentsList.setVisibility(View.VISIBLE);
            recyclerMain.setVisibility(View.GONE);
            populateTrainingFeed();
        }
    }

    @Override
    public void onClicked(View view, Document item) {

    }

    private void setAdapter(List<TrainingsResponse> trainingsResponses, List<Document> documents) {
        adapter = new RecyclerAdapter(getActivity(), trainingsResponses);
        myList.setLayoutManager(linearLayoutManager);
        myList.setAdapter(adapter);
        myList.setHasFixedSize(true);
        documentAdapter = new DocumentAdapter(getActivity(), documents, new DocumentAdapter.DocumentDownloadedListener() {
            @Override
            public void documentDownloaded(Integer questionID, String fileType) {
                hashMapDownloads.put(questionID, fileType);
                Log.i(Constants.TAG, "hashMapDocumentDownloaded: "+hashMapDownloads);
            }
        });
        documentsList.setLayoutManager(lm);
        documentsList.setAdapter(documentAdapter);
        documentsList.setHasFixedSize(true);
    }

    private void populateTrainingFeed() {
        setupPropertiesAdapter();
        if (trainingsResponses != null && trainingsResponses.size() > 0) {
            if (app.needToRefreshTrainings) {
                app.needToRefreshTrainings = false;
                getFeed(1);
            } else {
                setAdapter(trainingsResponses, documents);
            }
        } else {
            getFeed(1);
        }
    }

    private void setupPropertiesAdapter() {

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        lm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

//        myList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                if (dy > 0) //check for scroll down
//                {
//                    visibleItemCount = linearLayoutManager.getChildCount();
//                    totalItemCount = linearLayoutManager.getItemCount();
//                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
//
//                    if (loading) {
//                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
//                            loading = false;
//                            if (morePages && !isCallingApi) {
//                                currPage++;
//                                getFeed(currPage);
//                            }
//                        }
//                    }
//                }
//            }
//        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currPage = 1;
                morePages = true;
                getFeed(currPage);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);


    }


    private void getFeed(final int pageNumber) {
        loading = true;
        ImapApp.getApiService().getListTrainings(pageNumber).enqueue(new MyCallbackApi<List<TrainingsResponse>, BaseActivity>((BaseActivity) getActivity()) {

            @Override
            public void before() {
                if (emptyView == null) return;
                isCallingApi = true;
                emptyView.setVisibility(View.GONE);
                if (!swipeRefreshLayout.isRefreshing()) {
                    progressView.setVisibility(View.VISIBLE);
                }
                if (pageNumber == 1) {
                    myList.setVisibility(View.GONE);
                }
            }

            @Override
            public void after() {
                if (emptyView == null) return;
                isCallingApi = false;
                if (progressView != null && myList != null && swipeRefreshLayout != null) {
                    progressView.setVisibility(View.GONE);
                    myList.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onSuccess(Response<List<TrainingsResponse>> response) {
                if (getActivity() == null) return;
                trainingsResponses = response.body();
                getDocumentsList(trainingsResponses);
                if (trainingsResponses.size() == 0) {
                    morePages = false;
                }
                setAdapter(trainingsResponses, documents);
            }

            @Override
            public void onUnexpectedError(Call<List<TrainingsResponse>> call, Throwable t) {
                ((BaseActivity) getActivity()).handleUnexpectedError(t);
            }
        });
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        if (app.needToRefreshTrainings) {
//            getFeed(1);
//            app.needToRefreshTrainings = false;
//        }
//    }
}
