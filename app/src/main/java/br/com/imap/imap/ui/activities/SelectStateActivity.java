package br.com.imap.imap.ui.activities;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.enums.StateEnum;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.RescuePointsResponse;
import br.com.imap.imap.ui.adapters.spinner.StateSpinnerAdapter;
import br.com.imap.imap.ui.home.BaseActivity;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Response;

@EActivity(R.layout.activity_select_state)
public class SelectStateActivity extends BaseActivity {

    @ViewById(R.id.spinner_state)
    MaterialSpinner spinnerStates;

    @ViewById(R.id.btn_proximo)
    Button btnProximo;

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;

    @Extra(Constants.EXTRA.PRODUCT_ID)
    String productId;

    @Extra(Constants.EXTRA.PRODUCT_IMAGE)
    String productImage;

    String[] statesNames;

    List<RescuePointsResponse> rescuePoints;

    StateSpinnerAdapter stateAdapter;

    @AfterViews
    public void afterViews() {
        setupToolbar(getString(R.string.selecionar_estado), true, false);
        fillStateSpinner();
    }

    private void fillStateSpinner() {
        ImapApp.getApiService().getRescuePoints().enqueue(new MyCallbackApi<List<RescuePointsResponse>, BaseActivity>(this) {

            @Override
            public void after() {
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void before() {
                progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(Response<List<RescuePointsResponse>> response) {
                if (spinnerStates == null) return;
                rescuePoints = response.body();
                statesNames = new String[]{StateEnum.MG.getName(), StateEnum.SP.getName(), StateEnum.RJ.getName(), StateEnum.ES.getName(), StateEnum.BA.getName()
                        , StateEnum.CE.getName(), StateEnum.SE.getName(), StateEnum.PR.getName(), StateEnum.SC.getName(), StateEnum.RS.getName()};
                ArrayAdapter<String> conditionionAdapter = new ArrayAdapter<>(SelectStateActivity.this, R.layout.spinner_item, statesNames);
                conditionionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerStates.setAdapter(conditionionAdapter);
                EventBus.getDefault().postSticky(rescuePoints);
            }

            @Override
            public void onUnexpectedError(Call<List<RescuePointsResponse>> call, Throwable t) {

            }
        });
    }

    @Click(R.id.btn_proximo)
    public void onNextClicked(){
        SelectCityActivity_.intent(this)
                .extra(Constants.EXTRA.PRODUCT_ID, productId)
                .extra(Constants.EXTRA.PRODUCT_IMAGE, productImage)
                .extra(Constants.EXTRA.STATE, StateEnum.getValueByName(spinnerStates.getSelectedItem().toString()))
                .start();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
