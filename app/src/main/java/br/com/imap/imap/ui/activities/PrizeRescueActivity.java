package br.com.imap.imap.ui.activities;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.models.response.ProductsResponse;
import br.com.imap.imap.ui.home.BaseActivity;
import br.com.imap.imap.utils.GlideUtils;

@EActivity(R.layout.activity_prize_rescue)
public class PrizeRescueActivity extends BaseActivity {

    @Extra
    ProductsResponse product;

    @App
    ImapApp app;

    @ViewById(R.id.iv_imagem_premio)
    ImageView ivImagemPremio;

    @ViewById(R.id.tv_disponibilidade)
    TextView tvDisponibilidade;

    @ViewById(R.id.tv_produtos_ponto)
    TextView tvProdutosPonto;

    @ViewById(R.id.tv_premio_description)
    TextView tvPremioDescription;

    @ViewById(R.id.tv_meus_pontos)
    TextView tvMeusPontos;

    @ViewById(R.id.tv_produtos_pontos)
    TextView tvProdutosPontos;

    @ViewById(R.id.btn_resgatar_premio)
    Button btnResgatarPremio;

    @AfterViews
    public void afterViews() {
        setupToolbar(product.getName(), true, false);
        fillFields();
    }

    private void fillFields() {
        GlideUtils.loadImage(product.getPicture().getUrl(), this, ivImagemPremio, R.drawable.product_rescue_place_holder);
        tvDisponibilidade.setText(getString(R.string.quantidade_disponivel).replace("{x}", product.getQuantity().toString()));
        tvProdutosPonto.setText(getString(R.string.pontos).replace("{x}", product.getPoints().toString()));
        tvPremioDescription.setText(product.getDescription());
        tvMeusPontos.setText(getString(R.string.pontos).replace("{x}", app.getCurrentUser().getPoints().toString()));
        tvProdutosPontos.setText(getString(R.string.pontos).replace("{x}", product.getPoints().toString()));
    }

    @Click(R.id.btn_resgatar_premio)
    public void onRescueClicked(){
        SelectStateActivity_.intent(this)
                .extra(Constants.EXTRA.PRODUCT_ID, product.getId().toString())
                .extra(Constants.EXTRA.PRODUCT_IMAGE, product.getPicture().getUrl())
                .start();
    }

}
