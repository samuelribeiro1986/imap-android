package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.imap.imap.custom.ViewWrapper;
import br.com.imap.imap.interfaces.RecyclerViewItemClick;
import br.com.imap.imap.service.models.response.NotificationsResponse;
import br.com.imap.imap.ui.adapters.viewholders.NotificationsViewHolder;
import br.com.imap.imap.ui.adapters.viewholders.NotificationsViewHolder_;

/**
 * Created by Samuel Ribeiro on 10/01/2017.
 */
@EBean
public class NotificationsAdapter extends RecyclerViewAdapterBase<NotificationsResponse, NotificationsViewHolder> {

    @RootContext
    Context context;
    private RecyclerViewItemClick<NotificationsResponse> listener;

    public void setOnItemClickListener(@NonNull RecyclerViewItemClick<NotificationsResponse> listener){
        this.listener = listener;
    }

    @Override
    protected NotificationsViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return NotificationsViewHolder_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<NotificationsViewHolder> holder, int position) {
        NotificationsViewHolder view = holder.getView();
        final NotificationsResponse notifications = items.get(position);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        view.bind(notifications);
    }
}
