package br.com.imap.imap.service.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

public class Rescues extends BaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product")
    @Expose
    private ProductsResponse product;
    @SerializedName("rescue_point")
    @Expose
    private RescuePoint rescuePoint;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductsResponse getProduct() {
        return product;
    }

    public void setProduct(ProductsResponse product) {
        this.product = product;
    }

    public RescuePoint getRescuePoint() {
        return rescuePoint;
    }

    public void setRescuePoint(RescuePoint rescuePoint) {
        this.rescuePoint = rescuePoint;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}