package br.com.imap.imap.interfaces;

/**
 * Created by Samuel Ribeiro on 03/01/2017.
 */

public interface LoginDialogCallback {
    void loggedIn();

    void loggingFailed();
}
