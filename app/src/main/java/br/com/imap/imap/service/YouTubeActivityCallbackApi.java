package br.com.imap.imap.service;

import com.google.android.youtube.player.YouTubeBaseActivity;

import java.io.IOException;

import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Samuel on 08/01/2017.
 */

public abstract class YouTubeActivityCallbackApi<T, A extends YouTubeBaseActivity> implements Callback<T> {
    A activity;

    public YouTubeActivityCallbackApi(A context) {
        this.activity = context;
        before();
    }

    public abstract void onSuccess(Response<T> response);

    public abstract void onUnexpectedError(Call<T> call, Throwable t);


    public void before() {

    }

    public void after() {

    }

}
