package br.com.imap.imap.ui.activities;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.request.Rescue;
import br.com.imap.imap.service.models.request.RescueRequest;
import br.com.imap.imap.service.models.response.RescuePointsResponse;
import br.com.imap.imap.service.models.response.Rescues;
import br.com.imap.imap.ui.home.BaseActivity;
import br.com.imap.imap.utils.GlideUtils;
import retrofit2.Call;
import retrofit2.Response;

@EActivity(R.layout.activity_receipt)
public class ReceiptActivity extends BaseActivity {

    @ViewById(R.id.iv_prize_image)
    ImageView ivPrizeImage;

    @ViewById(R.id.tv_address)
    TextView tvAddress;

    @ViewById(R.id.progress_view)
    CircularProgressView progressView;

    @Extra(Constants.EXTRA.PRODUCT_ID)
    String productId;

    @Extra(Constants.EXTRA.PRODUCT_IMAGE)
    String productImage;

    @Extra(Constants.EXTRA.RESCUE_POINTS)
    RescuePointsResponse rescuesPoints;

    Rescue rescue;

    @AfterViews
    public void afterViews() {
        setupToolbar(getString(R.string.comprovante), true, false);
        fillFields();
        rescuePrize();
    }

    private void fillFields() {
        GlideUtils.loadImage(productImage, this, ivPrizeImage, R.drawable.product_rescue_place_holder);
        tvAddress.setText(rescuesPoints.getAddress());
    }

    public void rescuePrize() {
        rescue = new Rescue(productId, rescuesPoints.getId().toString());
        ImapApp.getApiService().rescuePrize(new RescueRequest(rescue)).enqueue(new MyCallbackApi<Rescues, BaseActivity>(this) {

            @Override
            public void after() {
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void before() {
                progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(Response<Rescues> response) {
                Log.i(Constants.TAG, "onSuccess: " + response);
            }

            @Override
            public void onUnexpectedError(Call<Rescues> call, Throwable t) {
                Log.e(Constants.TAG, "onSuccess: " + t);
            }
        });
    }
}
