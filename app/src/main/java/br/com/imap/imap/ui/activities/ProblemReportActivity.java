package br.com.imap.imap.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.request.Ocurrence;
import br.com.imap.imap.ui.home.BaseActivity;
import br.com.imap.imap.ui.home.HomeActivity_;
import br.com.imap.imap.utils.DialogUtils;
import br.com.imap.imap.utils.FileUtils;
import br.com.imap.imap.utils.ImageUtils;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Response;

@EActivity(R.layout.activity_problem_report)
public class ProblemReportActivity extends BaseActivity implements Validator.ValidationListener {

    @InstanceState
    Uri pictureUri;
    @InstanceState
    Uri croppedPictureUri;
    @ViewById(R.id.edt_problem_local)
    @NotEmpty
    TextView edtProblemLocal;
    @ViewById(R.id.edt_problem_report)
    @NotEmpty
    TextView edtProblemReport;
    @ViewById(R.id.btn_send_image)
    Button btnSendImage;
    @ViewById(R.id.btn_send_report)
    Button btnSendReport;
    Validator validator;
    Ocurrence ocurrence;

    boolean activityResultCalled = false;


    @AfterViews
    public void afterViews() {
        setupToolbar(getString(R.string.reportar_problema), true, false);
        ocurrence = new Ocurrence();
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Click(R.id.btn_send_image)
    public void imageClick() {
        DialogUtils.ChoiceDialog(this, new String[]{getString(R.string.gallery), getString(R.string.camera)}, getString(R.string.choose_an_option),
                new MaterialDialog.ListCallbackSingleChoice() {

                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (which == 0) {
                            ImageUtils.pickPhotoFromGallery(ProblemReportActivity.this);
                        } else {
                            Dexter.checkPermissions(new MultiplePermissionsListener() {

                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {

                                    if (report.areAllPermissionsGranted()) {

                                        pictureUri = ImageUtils.openCameraIntent(ProblemReportActivity.this);
                                    }
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        }
                        return true;
                    }
                });
    }

    @OnActivityResult(Constants.REQUEST_CAMERA)
    void onCameraResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            startCrop();
        }
    }


    @OnActivityResult(Constants.REQUEST_GALLERY)
    void onGalleryResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            pictureUri = data.getData();
            startCrop();
        }
    }

    private void startCrop() {
        activityResultCalled = true;
        CropImage.activity(pictureUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(ProblemReportActivity.this);
    }


    @OnActivityResult(CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
    void onCropResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            croppedPictureUri = result.getUri();
        }
    }

    @Override
    public void onValidationSucceeded() {
        if (croppedPictureUri != null) {
            ocurrence.setPicture(FileUtils.getFile(this, croppedPictureUri));
        }
        ocurrence.setLocal(edtProblemLocal.getText().toString());
        ocurrence.setLocal(edtProblemReport.getText().toString());
        sendReport();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Log.e(Constants.TAG, "onValidationFailed: " + errors);
    }

    public void sendReport() {
        ImapApp.getApiService().createOcurrence(ocurrence.getMultipart(), ocurrence.getMultipartPhoto()).enqueue(new MyCallbackApi<Ocurrence, BaseActivity>(this) {
            @Override
            public void onSuccess(Response<Ocurrence> response) {
                showSuccessDialog();
            }

            @Override
            public void onUnexpectedError(Call<Ocurrence> call, Throwable t) {

            }
        });
    }

    private MaterialDialog showSuccessDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(ProblemReportActivity.this)
                .buttonsGravity(GravityEnum.CENTER)
                .titleGravity(GravityEnum.START)
                .widgetColorRes(R.color.colorPrimaryDark)
                .cancelable(true)
                .customView(R.layout.dialog_report_success, false)
                .show();

        Button ok = (Button) dialog.findViewById(R.id.btn_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                HomeActivity_.intent(ProblemReportActivity.this)
                        .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .start();
            }
        });

        return dialog;
    }

    @Click(R.id.btn_send_report)
    public void sendClicked() {
        validator.validate();
    }
}
