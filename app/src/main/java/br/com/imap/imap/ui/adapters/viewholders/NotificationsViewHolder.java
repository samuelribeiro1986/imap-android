package br.com.imap.imap.ui.adapters.viewholders;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.NotificationsResponse;
import br.com.imap.imap.utils.GlideUtils;

/**
 * Created by Samuel Ribeiro on 10/01/2017.
 */
@EViewGroup(R.layout.adapter_notificacoes)
public class NotificationsViewHolder extends RelativeLayout {

    Context context;
    @ViewById(R.id.iv_imagem_notificacao)
    ImageView ivImagemNotificacao;
    @ViewById(R.id.tv_titulo_notificacao)
    TextView tvTituloNotificacao;
    @ViewById(R.id.tv_mensagem_notificacao)
    TextView tvMensagemNotificacao;

    public NotificationsViewHolder(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(NotificationsResponse notificationsResponse){
//        GlideUtils.loadImage(notificationsResponse.getPicture().getUrl(), context,ivImagemNotificacao, R.drawable.user_image_place_holder);
        tvTituloNotificacao.setText(notificationsResponse.getTitle());
        tvMensagemNotificacao.setText(notificationsResponse.getAlert());
    }
}
