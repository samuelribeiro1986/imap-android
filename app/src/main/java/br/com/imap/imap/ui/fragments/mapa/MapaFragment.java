package br.com.imap.imap.ui.fragments.mapa;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.service.MyCallbackApi;
import br.com.imap.imap.service.models.response.MapPointsResponse;
import br.com.imap.imap.ui.activities.ProblemReportActivity_;
import br.com.imap.imap.ui.home.BaseActivity;
import retrofit2.Call;
import retrofit2.Response;

@EFragment(R.layout.fragment_mapa)
public class MapaFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener, LocationListener {

    @ViewById(R.id.mapView)
    MapView mapView;

    @ViewById(R.id.btn_reportar_problema)
    Button btnReportarProblema;

    private GoogleApiClient mGoogleApiClient;

    private GoogleMap mMap;

    private MarkerOptions marker;

    private List<MapPointsResponse> mapPointsList;

    private synchronized void callConnection() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addOnConnectionFailedListener(MapaFragment.this)
                .addConnectionCallbacks(MapaFragment.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public static MapaFragment newInstance() {
        Bundle args = new Bundle();
        MapaFragment fragment = new MapaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    public void afterViews() {

        mapView.onCreate(null);
        mapView.onResume();
        mapView.getMapAsync(this);

        callConnection();
    }

    @Click(R.id.btn_reportar_problema)
    public void reportProblem(){
        ProblemReportActivity_.intent(getContext()).start();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location != null) {

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        String[] markerDetails = getMarkerDetails(marker.getTitle());
        View pointInfoWindow = getActivity().getLayoutInflater().inflate(R.layout.layout_point_info_content, null);
        TextView tvPointTitle = (TextView) pointInfoWindow.findViewById(R.id.tv_point_title);
        tvPointTitle.setText(markerDetails[1]);
        TextView tvPointDescription = (TextView) pointInfoWindow.findViewById(R.id.tv_point_description);
        TextView tvPointAddress = (TextView) pointInfoWindow.findViewById(R.id.tv_point_address);
        tvPointAddress.setText(markerDetails[2]);

        return pointInfoWindow;
    }

    private String[] getMarkerDetails(String details){
        String[] markerDetails = details.split("/");

        return markerDetails;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.mMap = googleMap;

        double latitude = 17.385044;

        double longitude = 78.486671;

        mMap.setInfoWindowAdapter(this);

        mMap.setOnInfoWindowClickListener(this);

        // create marker
        marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("Hello Maps");

        mMap.addMarker(marker);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(-19.922791, -43.944948)).zoom(12).build();
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        getMapPoints();

    }

    public void setPins(List<MapPointsResponse> mapPointsResponse){

        for (MapPointsResponse e : mapPointsResponse) {

            //Exibe mapa
            double lat = e.getLatitude();
            double lon = e.getLongitude();

            MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lon)).title(e.getName()+"/"+e.getCity()+"/"+e.getAddress());
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_all));
            mMap.addMarker(marker);

        }

    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    public void getMapPoints(){
        ImapApp.getApiService().getMapPoints().enqueue(new MyCallbackApi<List<MapPointsResponse>, BaseActivity>(((BaseActivity) getActivity())) {

            @Override
            public void after() {
                super.after();
            }

            @Override
            public void before() {
                super.before();
            }

            @Override
            public void onSuccess(Response<List<MapPointsResponse>> response) {
                mapPointsList = response.body();
                setPins(mapPointsList);
            }

            @Override
            public void onUnexpectedError(Call<List<MapPointsResponse>> call, Throwable t) {

            }
        });
    }
}
