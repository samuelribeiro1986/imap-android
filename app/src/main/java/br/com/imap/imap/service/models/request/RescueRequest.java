package br.com.imap.imap.service.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

public class RescueRequest extends BaseModel{

    @SerializedName("rescue")
    @Expose
    private Rescue rescue;

    public RescueRequest(Rescue rescue) {
        this.rescue = rescue;
    }

    public Rescue getRescue() {
        return rescue;
    }

    public void setRescue(Rescue rescue) {
        this.rescue = rescue;
    }

}

