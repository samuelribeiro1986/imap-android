package br.com.imap.imap.service.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.imap.imap.service.models.BaseModel;

/**
 * Created by Samuel Ribeiro on 04/01/2017.
 */

public class UserAnswers extends BaseModel {

    @SerializedName("answers_count")
    @Expose
    private Integer answersCount;

    @SerializedName("answer_correct")
    @Expose
    private Boolean answerCorrect;

    public Integer getAnswersCount() {
        return answersCount;
    }

    public void setAnswersCount(Integer answersCount) {
        this.answersCount = answersCount;
    }

    public Boolean getAnswerCorrect() {
        return answerCorrect;
    }

    public void setAnswerCorrect(Boolean answerCorrect) {
        this.answerCorrect = answerCorrect;
    }
}
