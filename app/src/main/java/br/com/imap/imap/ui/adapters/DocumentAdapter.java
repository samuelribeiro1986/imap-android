package br.com.imap.imap.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.imap.imap.R;
import br.com.imap.imap.service.models.response.Document;

/**
 * Created by Samuel on 07/01/2017.
 */
public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.DocumentHolder> {

    Context ctx;
    List<Document> documents;

    private DocumentDownloadedListener listener;
    public DocumentAdapter(Context context, List<Document> documents, DocumentDownloadedListener listener) {
        this.ctx = context;
        this.documents = documents;
        this.listener = listener;
    }

    @Override
    public DocumentAdapter.DocumentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_document, parent, false);
        return new DocumentAdapter.DocumentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DocumentHolder holder, final int position) {

        if (getFileType(documents.get(position).getFile().getUrl()).equals(ctx.getString(R.string.pdf))) {
            Glide.with(ctx).load(R.drawable.ic_pdf).into(holder.ivDocumentType);
        } else {
            Glide.with(ctx).load(R.drawable.ic_xls).into(holder.ivDocumentType);
        }
        holder.tvDocumentTitle.setText(documents.get(position).getName());
        holder.tvDocumentDescription.setText(documents.get(position).getDescription());
        holder.llItemDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.documentDownloaded(documents.get(holder.getAdapterPosition()).getId()
                        , getFileType(documents.get(holder.getAdapterPosition()).getFile().getUrl()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return documents.size();
    }

    public String getFileType(String url) {
        String segments[] = url.split("\\.");
        return segments[1];
    }

    public interface DocumentDownloadedListener {
        void documentDownloaded(Integer questionID, String fileType);
    }

    public class DocumentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivDocumentType;
        TextView tvDocumentTitle;
        TextView tvDocumentDescription;
        LinearLayout llItemDocument;

        public DocumentHolder(View itemView) {
            super(itemView);
            ivDocumentType = (ImageView) itemView.findViewById(R.id.iv_document_type);
            tvDocumentTitle = (TextView) itemView.findViewById(R.id.tv_document_title);
            tvDocumentDescription = (TextView) itemView.findViewById(R.id.tv_document_description);
            llItemDocument = (LinearLayout) itemView.findViewById(R.id.ll_item_document);
        }

        @Override
        public void onClick(View view) {


        }
    }

}
