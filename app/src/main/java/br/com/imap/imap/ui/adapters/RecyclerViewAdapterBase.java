package br.com.imap.imap.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.imap.imap.custom.ViewWrapper;

/**
 * Created by Samuel Ribeiro on 04/01/2017.
 */

public abstract class RecyclerViewAdapterBase<T, V extends View> extends RecyclerView.Adapter<ViewWrapper<V>> {

    List<T> items = new ArrayList<>();
    List<T> itemsCopy = new ArrayList<>();

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public final ViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper<>(onCreateItemView(parent, viewType));
    }

    protected abstract V onCreateItemView(ViewGroup parent, int viewType);


    public void setData(List<T> items) {
        this.items.clear();
        this.items.addAll(items);
        this.itemsCopy.clear();
        this.itemsCopy.addAll(items);
        this.notifyDataSetChanged();
    }


    public void addData(List<T> items) {
        this.items.addAll(items);
        this.itemsCopy.addAll(items);
        this.notifyDataSetChanged();
    }

    public List<T> getItems() {
        return items;
    }
// additional methods to manipulate the items

}
