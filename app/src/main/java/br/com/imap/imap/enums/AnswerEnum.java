package br.com.imap.imap.enums;

/**
 * Created by Samuel Ribeiro on 11/01/2017.
 */

public enum AnswerEnum {

    A(0, "A"),
    B(1, "B"),
    C(2, "C"),
    D(3, "D");

    private int id;
    private String value;

    AnswerEnum(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public static String getNameById(int id) {
        for (AnswerEnum c :
                AnswerEnum.values()) {
            if (c.getValue().equals(id)) return c.getValue();
        }
        return A.getValue();
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
