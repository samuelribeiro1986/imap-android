package br.com.imap.imap.utils;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import br.com.imap.imap.app.Constants;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Samuel on 07/01/2017.
 */

public class GlideUtils {

    public static void loadImageCropCircle(Object url, Context with, ImageView into, int placeHolder) {
        if (with == null) {
            return;
        }
        try {
            Glide.with(with).load(url).placeholder(placeHolder)
                    .bitmapTransform(new CropCircleTransformation(with))
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(into);
        } catch (Exception ignored) {

        }
    }


    public static void loadImageCropCircleWithRefresh(Object url, Context with, final CircleImageView into, int placeHolder, int width, int height) {
        if (with == null) {
            return;
        }
        Glide.with(with)
                .load(url)
                .bitmapTransform(new CropCircleTransformation(with))
                .placeholder(placeHolder)
                .dontAnimate()
                .into(new SimpleTarget<GlideDrawable>(width, height) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        into.setImageDrawable(resource);
                    }
                });
    }


    public static void loadImageCropCircleIntoTextViewDrawable(Object url, Context with, final TextView into, int width, int height) {
        if (with == null) {
            return;
        }
        Glide.with(with)
                .load(url)
                .bitmapTransform(new CropCircleTransformation(with))
                .into(new SimpleTarget<GlideDrawable>(width, height) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        into.setCompoundDrawablesWithIntrinsicBounds(resource, null, null, null);
                    }
                });
    }


    public static void loadImage(Object url, Context with, final ImageView into, int placeHolder, int width, int height) {
        if (with == null) {
            return;
        }
        Glide.with(with)
                .load(url)
                .placeholder(placeHolder)
                .into(new SimpleTarget<GlideDrawable>(width, height) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        into.setBackground(resource);
                    }
                });
    }


    public static void loadImage(Object url, Context context, final ImageView into, int placeHolder) {
        if (context == null) {
            return;
        }
        Glide.with(context)
                .load(Constants.HOST + url)
                .placeholder(placeHolder)
                .centerCrop()
                .into(into);
    }

    public static void loadImage(Object url, Context context, final ImageView into) {
        if (context == null) {
            return;
        }
        Glide.with(context)
                .load(Constants.HOST + url)
                .centerCrop()
                .into(into);
    }

    public static void loadLocalImage(Object url, int placeHolder, Context with, final ImageView into) {
        if (with == null) {
            return;
        }
        Glide.with(with)
                .load(url)
                .placeholder(placeHolder)
                .centerCrop()
                .into(into);
    }
}
