package br.com.imap.imap.ui.fragments.training;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.imap.imap.R;
import br.com.imap.imap.app.Constants;
import br.com.imap.imap.app.ImapApp;
import br.com.imap.imap.ui.adapters.TrainingViewPagerAdapter;

@EFragment(R.layout.fragment_training)
public class TrainingFragment extends Fragment {

    @ViewById(R.id.profile_view_pager)
    ViewPager profileViewPager;

    @ViewById(R.id.trainings_tabs)
    TabLayout profileTabLayout;

    @App
    ImapApp app;

    @AfterViews
    public void afterView(){

        setupViewPager(profileViewPager);
        profileTabLayout.setupWithViewPager(profileViewPager);
    }

    public void setupViewPager(ViewPager viewPager) {
        TrainingViewPagerAdapter trainingViewPagerAdapter = new TrainingViewPagerAdapter(getChildFragmentManager());
        trainingViewPagerAdapter.addFragment(LessonsFragment_.builder().trainingsFragmentType(Constants.VIDEOS).build(), getString(R.string.videos));
        trainingViewPagerAdapter.addFragment(LessonsFragment_.builder().trainingsFragmentType(Constants.DOCUMENTS).build(), getString(R.string.documentos));
        viewPager.setAdapter(trainingViewPagerAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getChildFragmentManager() != null && getChildFragmentManager().getFragments() != null) {
            for (Fragment fragment : getChildFragmentManager().getFragments()) {
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

}
